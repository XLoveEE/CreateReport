﻿namespace CreateReport
{
    partial class Report
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report));
            this.label1 = new System.Windows.Forms.Label();
            this.tb_dire = new System.Windows.Forms.TextBox();
            this.btn_dire = new System.Windows.Forms.Button();
            this.dtp_start = new System.Windows.Forms.DateTimePicker();
            this.dtp_end = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Upload = new System.Windows.Forms.Button();
            this.btn_search = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dgv_beamnums = new System.Windows.Forms.DataGridView();
            this.beamnums = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grid_report = new FlexCell.Grid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_beamnums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(194, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "选择数据库:";
            // 
            // tb_dire
            // 
            this.tb_dire.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tb_dire.Location = new System.Drawing.Point(307, 31);
            this.tb_dire.Name = "tb_dire";
            this.tb_dire.Size = new System.Drawing.Size(472, 29);
            this.tb_dire.TabIndex = 1;
            // 
            // btn_dire
            // 
            this.btn_dire.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_dire.Location = new System.Drawing.Point(785, 32);
            this.btn_dire.Name = "btn_dire";
            this.btn_dire.Size = new System.Drawing.Size(62, 27);
            this.btn_dire.TabIndex = 2;
            this.btn_dire.Text = "......";
            this.btn_dire.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_dire.UseVisualStyleBackColor = true;
            this.btn_dire.Click += new System.EventHandler(this.btn_dire_Click);
            // 
            // dtp_start
            // 
            this.dtp_start.CalendarFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtp_start.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtp_start.Location = new System.Drawing.Point(305, 64);
            this.dtp_start.Name = "dtp_start";
            this.dtp_start.Size = new System.Drawing.Size(200, 29);
            this.dtp_start.TabIndex = 3;
            // 
            // dtp_end
            // 
            this.dtp_end.CalendarFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtp_end.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtp_end.Location = new System.Drawing.Point(578, 66);
            this.dtp_end.Name = "dtp_end";
            this.dtp_end.Size = new System.Drawing.Size(200, 29);
            this.dtp_end.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(524, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = ">>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(194, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 21);
            this.label3.TabIndex = 6;
            this.label3.Text = "张 拉 日 期:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_Upload);
            this.groupBox1.Controls.Add(this.btn_search);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_dire);
            this.groupBox1.Controls.Add(this.dtp_end);
            this.groupBox1.Controls.Add(this.btn_dire);
            this.groupBox1.Controls.Add(this.dtp_start);
            this.groupBox1.Location = new System.Drawing.Point(12, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1277, 120);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // btn_Upload
            // 
            this.btn_Upload.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Upload.Location = new System.Drawing.Point(1056, 34);
            this.btn_Upload.Name = "btn_Upload";
            this.btn_Upload.Size = new System.Drawing.Size(86, 59);
            this.btn_Upload.TabIndex = 11;
            this.btn_Upload.Text = "上传数据";
            this.btn_Upload.UseVisualStyleBackColor = true;
            this.btn_Upload.Visible = false;
            this.btn_Upload.Click += new System.EventHandler(this.btn_Upload_Click);
            // 
            // btn_search
            // 
            this.btn_search.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_search.Location = new System.Drawing.Point(785, 66);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(62, 27);
            this.btn_search.TabIndex = 7;
            this.btn_search.Text = "检索";
            this.btn_search.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.Location = new System.Drawing.Point(928, 34);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 59);
            this.button2.TabIndex = 10;
            this.button2.Text = "报表输出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dgv_beamnums
            // 
            this.dgv_beamnums.AllowUserToResizeColumns = false;
            this.dgv_beamnums.AllowUserToResizeRows = false;
            this.dgv_beamnums.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_beamnums.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_beamnums.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_beamnums.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.beamnums});
            this.dgv_beamnums.GridColor = System.Drawing.Color.White;
            this.dgv_beamnums.Location = new System.Drawing.Point(12, 215);
            this.dgv_beamnums.MultiSelect = false;
            this.dgv_beamnums.Name = "dgv_beamnums";
            this.dgv_beamnums.ReadOnly = true;
            this.dgv_beamnums.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dgv_beamnums.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_beamnums.RowTemplate.Height = 23;
            this.dgv_beamnums.Size = new System.Drawing.Size(172, 344);
            this.dgv_beamnums.TabIndex = 8;
            this.dgv_beamnums.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_beamnums_CellClick);
            this.dgv_beamnums.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_beamnums_CellContentClick);
            this.dgv_beamnums.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_beamnums_CellDoubleClick);
            this.dgv_beamnums.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dgv_beamnums_KeyUp);
            // 
            // beamnums
            // 
            this.beamnums.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.beamnums.HeaderText = "梁   号";
            this.beamnums.Name = "beamnums";
            this.beamnums.ReadOnly = true;
            // 
            // grid_report
            // 
            this.grid_report.BorderStyle = FlexCell.BorderStyleEnum.Light3D;
            this.grid_report.CheckedImage = null;
            this.grid_report.DefaultFont = new System.Drawing.Font("宋体", 9F);
            this.grid_report.ExtendLastCol = true;
            this.grid_report.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.grid_report.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.grid_report.Location = new System.Drawing.Point(190, 216);
            this.grid_report.Name = "grid_report";
            this.grid_report.SelectionMode = FlexCell.SelectionModeEnum.ByRow;
            this.grid_report.Size = new System.Drawing.Size(1104, 343);
            this.grid_report.TabIndex = 9;
            this.grid_report.UncheckedImage = null;
            this.grid_report.Load += new System.EventHandler(this.grid_report_Load);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(526, 84);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 42F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(277, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(760, 75);
            this.label4.TabIndex = 13;
            this.label4.Text = "预应力智能张拉数据管理系统";
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1315, 614);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.grid_report);
            this.Controls.Add(this.dgv_beamnums);
            this.Controls.Add(this.groupBox1);
            this.Name = "Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " 张拉数据查询及报表输出";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Report_FormClosed);
            this.Load += new System.EventHandler(this.Report_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Report_KeyUp);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_beamnums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_dire;
        private System.Windows.Forms.Button btn_dire;
        private System.Windows.Forms.DateTimePicker dtp_start;
        private System.Windows.Forms.DateTimePicker dtp_end;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.DataGridView dgv_beamnums;
        private System.Windows.Forms.DataGridViewTextBoxColumn beamnums;
        private FlexCell.Grid grid_report;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Upload;
    }
}

