﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OutExcelDll;

using RemoteDB_DLL;

using SqlDBOperationDll;
using Ini_Dll;
using OutReport;
namespace CreateReport
{
    public partial class Report : Form
    {
        ReportOutput excel = new ReportOutput();
        RemoteDB.beamInfor rebeaminfor = new RemoteDB.beamInfor();    
       

        private ProForm proForm;

        private string beamnum = "";
      
        public Report()
        {
            InitializeComponent();
        }

        private void GridLoatFile(string beamNum)
        {
            string path = System.Environment.CurrentDirectory;
            path = path.Insert(path.Length, "\\" + beamNum + ".flx");
            grid_report.OpenFile(path);
        }
      
        private void grid_report_Load(object sender, EventArgs e)
        {
            string path = System.Environment.CurrentDirectory;
            path = path.Insert(path.Length, "\\model_2.flx");
            grid_report.OpenFile(path);
            dgv_beamnums.AutoGenerateColumns = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            excel.SaveAsExel(this.beamnum);
            SaveFileDialog saveExcel = new SaveFileDialog();
            saveExcel.Filter = "Excel(*.xls)|*.xls";
            saveExcel.FileName = this.beamnum;
            saveExcel.FilterIndex = 1;
            if (saveExcel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string savePath = saveExcel.FileName;
                string srcPath = System.Environment.CurrentDirectory;
                srcPath = srcPath.Insert(srcPath.Length, "\\"+this.beamnum + ".xls");
                System.IO.File.Delete(savePath);
                System.IO.File.Move(srcPath, savePath);
                MessageBox.Show("报表输出完成!");
            }
           
        }

        private void Report_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;

            HotKey.RegisterHotKey(Handle, 100, HotKey.KeyModifiers.Ctrl,  Keys.M);
            string path = System.Environment.CurrentDirectory;
            path = path.Insert(path.Length, "\\张拉模式.ini");
            Ini ini = new Ini(path);
            string result = ini.ReadValue("远程数据库", "Enable");
            if (result == "on")
            {
                this.btn_Upload.Visible = true;
            }
            else
            {
                this.btn_Upload.Visible = false;
            }
        }

        private void btn_dire_Click(object sender, EventArgs e)
        {
            excel.CloseConnectDataBase();
            tb_dire.Text =  excel.GetDataBasePath();
            if (tb_dire.Text != "")
            {
                excel.ConnectDataBase();
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            if (tb_dire.Text != "")
            {
                DataTable dt_Beams = excel.GetBeamNums(dtp_start.Value, dtp_end.Value);
                if (dt_Beams != null && dt_Beams.Rows.Count > 0)
                {
                    dgv_beamnums.DataSource = null;
                    dgv_beamnums.DataSource = dt_Beams;
                    dgv_beamnums.Columns[0].DataPropertyName = "梁号";
                }
            }
        }

        private void Report_FormClosed(object sender, FormClosedEventArgs e)
        {
            HotKey.UnregisterHotKey(Handle, 100);
            excel.CloseConnectDataBase();
        }

        private void dgv_beamnums_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           // try
           // {
                int row = e.RowIndex;
                string beamselect = dgv_beamnums.Rows[row].Cells[0].Value.ToString();
                if (beamselect != null && beamselect != null)
                {
                    excel.CreateReportWithPressure(beamselect);
                    GridLoatFile(beamselect);
                    this.beamnum = beamselect;
                }
            //}
            //catch (NullReferenceException)
            //{
            //    MessageBox.Show("dgv_beamnums_CellDoubleClick");
            //}
        }

        ModifyDataForm form;
        protected override void WndProc(ref Message m)
        {
            const int WM_HOTKEY = 0x0312;
            //按快捷键 
            switch (m.Msg)
            {
                case WM_HOTKEY:
                    switch (m.WParam.ToInt32())
                    {
                        case 100:

                            if (form == null || form.IsDisposed)
                            {
                                form = new ModifyDataForm(excel.constr);
                                form.Show();
                            }
                            else
                            {
                                form.Activate();
                            }
                            break;
                    }
                    break;
            }
            base.WndProc(ref m);
        }

        private RemoteDB.beamInfor GetReBeamInfor(string beamname)
        {
            RemoteDB.beamInfor tmp = new RemoteDB.beamInfor();
            tmp.beanname = beamname;
            string selectString = "select * from Beams where 梁号 = '" + beamname + "'";
            DataTable dtResult = excel.sql.GetDataTable(selectString);
            if (dtResult != null && dtResult.Rows.Count > 0)
            {
                tmp.beammode = dtResult.Rows[0]["梁型"].ToString();
                tmp.stressdate = Convert.ToDateTime( dtResult.Rows[0]["张拉日期"]);
                tmp.designintensity = dtResult.Rows[0]["砼设计强度"].ToString();
                tmp.realintensity =(float)Convert.ToDouble( dtResult.Rows[0]["砼实际强度"]);
                tmp.percent1 = Convert.ToInt32(dtResult.Rows[0]["阶段一比例"]);
                tmp.percent2 = Convert.ToInt32(dtResult.Rows[0]["阶段二比例"]);
                try
                {
                    string imagename = dtResult.Rows[0]["图片名称"].ToString();
                    imagename = imagename.Remove(imagename.Length - 4, 4);
                    tmp.imagename = imagename;
                }
                catch (ArgumentOutOfRangeException)
                {
                }

                string path = System.Environment.CurrentDirectory;
                path = path.Insert(path.Length, "\\张拉模式.ini");
                Ini ini = new Ini(path);
                tmp.DeviceID = ini.ReadValue("设备ID", "设备ID");

                selectString = "select * from BeamInformation where 梁号 = '" + beamname + "' order by 序号";
                dtResult = excel.sql.GetDataTable(selectString);
                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            tmp.holename = dtResult.Rows[i]["孔道"].ToString();
                            tmp.designstress = dtResult.Rows[i]["设计张拉力"].ToString();
                            tmp.designlength = dtResult.Rows[i]["设计伸长量"].ToString();
                        }
                        else
                        {
                            tmp.holename += ","+dtResult.Rows[i]["孔道"].ToString();
                            tmp.designstress += "," + dtResult.Rows[i]["设计张拉力"].ToString();
                            tmp.designlength += "," + dtResult.Rows[i]["设计伸长量"].ToString();
                        }
                    }
                }
            
            }
            return tmp;
        }
        private DataTable GetReDataTable(string beamname)
        {
            string selectString = "select * from [" + beamname + "_KeyValue]";
            DataTable dtResult = excel.sql.GetDataTable(selectString);
            if (dtResult != null && dtResult.Rows.Count > 0)
            {
                return dtResult;
            }
            else
            {
                return null;
            }
        }
       
        private void btn_Upload_Click(object sender, EventArgs e)
        {
            if (tb_dire.Text != "")
            {
                DialogResult result = MessageBox.Show("是否上传数据", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    rebeaminfor = GetReBeamInfor(this.beamnum);
                    DataTable dtReData = GetReDataTable(this.beamnum);
                    proForm = new ProForm(rebeaminfor, dtReData);
                    proForm.ShowDialog();
                }
            }
        }



        private void Report_KeyUp(object sender, KeyEventArgs e)
        {
            //if (Keys.Delete == e.KeyCode)
            //{
            //    MessageBox.Show("Delete Key is Pressed!");
            //}
        }

        private void dgv_beamnums_KeyUp(object sender, KeyEventArgs e)
        {
            if (Keys.Delete == e.KeyCode)
            {
                if (this.beamnum != "")
                {
                    DialogResult dr = MessageBox.Show("是否删除梁号\""+this.beamnum+"\"的相关数据?","警告",MessageBoxButtons.YesNo,MessageBoxIcon.Warning,MessageBoxDefaultButton.Button2);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
                        SqlDBOperation dbo = new SqlDBOperation();
                       string   constr = "Data Source = " + this.tb_dire.Text+ ";Password=zzjnzlsj;";
                       dbo.Open(constr);

                       dbo.DeleteBeamRelatedInformation(beamnum);
                       DataTable dt_Beams = excel.GetBeamNums(dtp_start.Value, dtp_end.Value);
                       if (dt_Beams != null && dt_Beams.Rows.Count > 0)
                       {
                           dgv_beamnums.DataSource = null;
                           dgv_beamnums.DataSource = dt_Beams;
                           dgv_beamnums.Columns[0].DataPropertyName = "梁号";
                       }                    
                        dbo.Close();
                    }
                    else
                    {

                    }
                }
            }
        }

        private void dgv_beamnums_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int row = e.RowIndex;
                string beamselect = dgv_beamnums.Rows[row].Cells[0].Value.ToString();
                if (beamselect != null && beamselect != null)
                {
                    this.beamnum = beamselect;
                }
            }
            catch (NullReferenceException)
            {
                // MessageBox.Show("dgv_beamnums_CellDoubleClick");
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private void dgv_beamnums_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    
    }
}
