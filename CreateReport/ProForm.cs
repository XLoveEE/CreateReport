﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using RemoteDB_DLL;

namespace CreateReport
{
    public partial class ProForm : Form
    {

        private RemoteDB_DLL.RemoteDB.beamInfor rebeaminfor;
        private DataTable dtReData;
        private int proValue=0;
        RemoteDB upLoad = new RemoteDB(); 
      
        public ProForm()
        {
            InitializeComponent();
        }
     
        public ProForm(int max)
        {
            InitializeComponent();
            this.progressBar1.Maximum = max;
        }

        public ProForm(RemoteDB_DLL.RemoteDB.beamInfor rebeam,DataTable dtRedata)
        {
            InitializeComponent();
            this.rebeaminfor = rebeam;
            this.dtReData = dtRedata;
        }

        private void UpLoadReData()
        {
            RemoteDB_DLL.RemoteDB.RemoteData reData = new RemoteDB_DLL.RemoteDB.RemoteData();
            reData.beamname = rebeaminfor.beanname;
            reData.DeviceID = rebeaminfor.DeviceID;

            DataTable dtResult = this.dtReData;
            if (dtResult != null && dtResult.Rows.Count > 0)
            {
                this.progressBar1.Maximum = 100;//dtResult.Rows.Count;
                for (int i = 0; i < dtResult.Rows.Count; i++)
                {
                   reData.stationno = Convert.ToInt32(dtResult.Rows[i]["站号"]);
                   reData.times = Convert.ToInt32(dtResult.Rows[i]["张拉序号"])+1;
                   reData.stress = (float)Convert.ToDouble(dtResult.Rows[i]["张拉力"]);
                   reData.length = (float)Convert.ToDouble(dtResult.Rows[i]["伸长量"]);
                   reData.stage = Convert.ToInt32(dtResult.Rows[i]["比例"]);
                   reData.time = (float)Convert.ToDouble(dtResult.Rows[i]["张拉时间"]);
                    upLoad.InsertRemoteData(reData);
                    proValue = (int)(i*1.0/(dtResult.Rows.Count-1)*100.0);
                    try
                    {
                        this.bwProgressBar.ReportProgress(proValue);
                    }
                    catch (System.InvalidOperationException e)
                    {
                        MessageBox.Show(e.Message);
                    }
                    Thread.Sleep(100);
                }
                MessageBox.Show("数据上传完成!");
                this.bwProgressBar.CancelAsync();
           }
        }
        private void ProForm_Load(object sender, EventArgs e)
        {
            this.bwProgressBar.WorkerSupportsCancellation = true;
            this.bwProgressBar.WorkerReportsProgress = true;
            this.bwProgressBar.RunWorkerAsync();
        }

        public void SetProgressValue(int value)
        {
            this.progressBar1.Value = value;
            this.label1.Text = "正在上传数据......" + value.ToString() + "%";
            Console.WriteLine("ProValue is " + value);
        }

        private void bwProgressBar_DoWork(object sender, DoWorkEventArgs e)
        {
            this.bwProgressBar.ReportProgress(-1,"正在连接数据库.....");
            upLoad.Open();
            if (upLoad.IsOpen)
            {
                upLoad.DeleteRemoteBeamInfro(rebeaminfor);
                upLoad.DeleteRemoteDate(rebeaminfor.beanname,rebeaminfor.DeviceID);
              
                if (upLoad.InsertBeamInfor(rebeaminfor))
                {
                    UpLoadReData();
                }
                else
                {
                    this.bwProgressBar.CancelAsync();
                }
            }

            else
            {
                MessageBox.Show("数据库连接失败!");
                this.bwProgressBar.CancelAsync();
            }          
        }

        private void bwProgressBar_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage < 0)
            {
                this.label1.Text = e.UserState.ToString();
            }
            else
            {
                SetProgressValue(e.ProgressPercentage);
            }
        }

        private void bwProgressBar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }

        private void ProForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (upLoad.IsOpen == true)
            {
                upLoad.Close();
            }
            this.Dispose();
        }
    }
}
