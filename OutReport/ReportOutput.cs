﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GatherDataDll;
using SqlDBOperationDll;
using System.Windows.Forms;
using System.Data;
using FlexCell;
using Ini_Dll;

namespace OutReport
{
    public class ReportOutput 
    {
        struct JackInfor
        {
            public string jack_serial;
            public string jack_date;
            public string press_sense;
            public string jacn_section;
            public double a;
            public double b;
        }
        const int Col_100 = 8;
        public  SqlDBOperation sql = new SqlDBOperation();
        private string db_Path;
        private FlexCell.Grid grid = new FlexCell.Grid();
        private const int startRow_2 = 13;
        private const int Image_Rows = 8;
        private const int Image_Column = 11;
        private int currentRow;
        JackInfor[] jacks;
        private double retraceValue;
        private double jack_internal_length;
        private int firstScale;
        private int secondScale;
        GatherData gather;
        public string constr;
        public string DB_Path
        {
            get { return db_Path; }
        }

        public string GetDataBasePath()
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "sdf文件|*.sdf";
            if (open.ShowDialog() == DialogResult.OK)
            {
                db_Path = open.FileName;
                return db_Path;
            }
            else
            {
                return null;
            }
        }
        public void CloseConnectDataBase()
        {
            this.sql.Close();
 
        }
        public void ConnectDataBase()
        {
            constr = "Data Source = " + this.db_Path + ";Password=zzjnzlsj;";
            sql.Open(constr);
        }
        public DataTable GetBeamNums(DateTime startTime, DateTime endTime)
        {
            string selectString = "select 梁号 from Beams where 张拉日期 between '" + startTime.AddDays(-1).ToShortDateString() + "' and '" + endTime.AddDays(1).ToShortDateString() + "'";
            return sql.GetDataTable(selectString);
        }

        public void InsertRecordRows_1()
        {
            int offset = 0;
            int inrows = 0;
            if (gather.MaxTimes.Length > 5)
            {
                offset = 2;
            }
            try
            {
                int row = gather.Datas.Count;
                int col = gather.Datas[0].Count;
                inrows = row * col;
            }
            catch (IndexOutOfRangeException)
            {

            }
            for (int i = 0; i < inrows; i++)
            {
                FlexCell.Range range;
                //钢束编号
                range = grid.Range(currentRow, 1, currentRow + 1, 1);
                range.Merge();

                //张拉断面
                range = grid.Range(currentRow, 2, currentRow + 1, 2);
                range.Merge();

                //range = grid.Range(currentRow + 2, 2, currentRow + 1, 2);
                //range.Merge();

                grid.Cell(currentRow, 3).Text = "张拉力(KN)";
                grid.Cell(currentRow + 1, 3).Text = "伸长量(mm)";

                //设计张拉力
                range = grid.Range(currentRow, 9 + offset, currentRow + 1, 9 + offset);
                range.Merge();

                //range = grid.Range(currentRow + 2, 9, currentRow + 3, 9);
                //range.Merge();
                //总伸长量
                range = grid.Range(currentRow, 10 + offset, currentRow + 1, 10 + offset);
                range.Merge();

                //理论伸长量             
                range = grid.Range(currentRow, 11 + offset, currentRow + 1, 11 + offset);
                range.Merge();

                //延伸量误差
                range = grid.Range(currentRow, 12 + offset, currentRow + 1, 12 + offset);
                range.Merge();

                //断丝处理
                range = grid.Range(currentRow, 13 + offset, currentRow + 1, 13 + offset);
                range.Merge();
                range = grid.Range(currentRow, 1 + offset, currentRow + 1, 13 + offset);
                currentRow += 2;
            }     
        }
        public void InsertRecordRows_2()
        {
            int offset = 0;
            int inrows = 0;
            if (gather.MaxTimes.Length > 5)
            {
                offset = 2;
            }
            try
            {
                int row = gather.Datas.Count;
                int col = gather.Datas[0].Count;
                inrows = row * (col/2);
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
            for (int i = 0; i < inrows; i++)
            {
                FlexCell.Range range;
                //钢束编号
                range = grid.Range(currentRow, 1, currentRow + 3, 1);
                range.Merge();
                //张拉断面

                range = grid.Range(currentRow, 2, currentRow + 1, 2);
                range.Merge();

                range = grid.Range(currentRow + 2, 2, currentRow + 1, 2);
                range.Merge();

                grid.Cell(currentRow, 3).Text = "张拉力(KN)";
                grid.Cell(currentRow + 1, 3).Text = "伸长量(mm)";
                grid.Cell(currentRow + 2, 3).Text = "张拉力(KN)";
                grid.Cell(currentRow + 3, 3).Text = "伸长量(mm)";

                //设计张拉力
                range = grid.Range(currentRow, 9 + offset, currentRow + 3, 9 + offset);
                range.Merge();

                //range = grid.Range(currentRow + 2, 9, currentRow + 3, 9);
                //range.Merge();
                //总伸长量
                range = grid.Range(currentRow, 10 + offset, currentRow + 3, 10 + offset);
                range.Merge();

                //理论伸长量             
                range = grid.Range(currentRow, 11 + offset, currentRow + 3, 11 + offset);
                range.Merge();

                //延伸量误差
                range = grid.Range(currentRow, 12 + offset, currentRow + 3, 12 + offset);
                range.Merge();

                //断丝处理
                range = grid.Range(currentRow, 13 + offset, currentRow + 3, 13 + offset);
                range.Merge();
               // range = grid.Range(currentRow, 1, currentRow + 3, 13 + offset);
                currentRow += 4;
            } 
        }

        public void InsertRecordRows_1_pressure()
        {
            int offset = 0;
            int inrows = 0;
            if (gather.MaxTimes.Length > 5)
            {
                offset = 2;
            }
            try
            {
                int row = gather.Datas.Count;
                int col = gather.Datas[0].Count;
                inrows = row * col;
            }
            catch (IndexOutOfRangeException)
            {

            }
            for (int i = 0; i < inrows; i++)
            {
                FlexCell.Range range;
                //钢束编号
                range = grid.Range(currentRow, 1, currentRow + 2, 1);
                range.Merge();

                //张拉断面
                range = grid.Range(currentRow, 2, currentRow + 2, 2);
                range.Merge();

                //range = grid.Range(currentRow + 2, 2, currentRow + 1, 2);
                //range.Merge();

                grid.Cell(currentRow, 3).Text = "张拉力(KN)";
                grid.Cell(currentRow + 1, 3).Text = "压强值(MPa)";
                grid.Cell(currentRow + 2, 3).Text = "伸长量(mm)";

                //设计张拉力
                range = grid.Range(currentRow, 9 + offset, currentRow + 2, 9 + offset);
                range.Merge();

                //range = grid.Range(currentRow + 2, 9, currentRow + 3, 9);
                //range.Merge();
                //总伸长量
                range = grid.Range(currentRow, 10 + offset, currentRow + 2, 10 + offset);
                range.Merge();

                //理论伸长量             
                range = grid.Range(currentRow, 11 + offset, currentRow + 2, 11 + offset);
                range.Merge();

                //延伸量误差
                range = grid.Range(currentRow, 12 + offset, currentRow + 2, 12 + offset);
                range.Merge();

                //断丝处理
                range = grid.Range(currentRow, 13 + offset, currentRow + 2, 13 + offset);
                range.Merge();
               
                currentRow += 3;
            }
        }
        public void InsertRecordRows_2_pressure()
        {
            int offset = 0;
            int inrows = 0;
            if (gather.MaxTimes.Length > 5)
            {
                offset = 2;
            }
            try
            {
                int row = gather.Datas.Count;
                int col = gather.Datas[0].Count;
                inrows = row * (col / 2);
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
            for (int i = 0; i < inrows; i++)
            {
                FlexCell.Range range;
                //钢束编号
                range = grid.Range(currentRow, 1, currentRow + 5, 1);
                range.Merge();
                //张拉断面

                range = grid.Range(currentRow, 2, currentRow + 2, 2);
                range.Merge();

                range = grid.Range(currentRow + 3, 2, currentRow + 5, 2);
                range.Merge();

                grid.Cell(currentRow, 3).Text = "张拉力(KN)";
                grid.Cell(currentRow + 1, 3).Text = "压强值(MPa)";
                grid.Cell(currentRow + 2, 3).Text = "伸长量(mm)";
               
                grid.Cell(currentRow + 3, 3).Text = "张拉力(KN)";
                grid.Cell(currentRow + 4, 3).Text = "压强值(MPa)";
                grid.Cell(currentRow + 5, 3).Text = "伸长量(mm)";

                //设计张拉力
                range = grid.Range(currentRow, 9 + offset, currentRow + 5, 9 + offset);
                range.Merge();

                //range = grid.Range(currentRow + 2, 9, currentRow + 3, 9);
                //range.Merge();
                //总伸长量
                range = grid.Range(currentRow, 10 + offset, currentRow + 5, 10 + offset);
                range.Merge();

                //理论伸长量             
                range = grid.Range(currentRow, 11 + offset, currentRow + 5, 11 + offset);
                range.Merge();

                //延伸量误差
                range = grid.Range(currentRow, 12 + offset, currentRow + 5, 12 + offset);
                range.Merge();

                //断丝处理
                range = grid.Range(currentRow, 13 + offset, currentRow + 5, 13 + offset);
                range.Merge();
                // range = grid.Range(currentRow, 1, currentRow + 3, 13 + offset);
                currentRow += 6;
            }
        }
       
        public void MakeTableButtom()
        {
            FlexCell.Range range;
            grid.Cell(currentRow, 1).Text = "备注:";

            range = grid.Range(currentRow, 2, currentRow, 13);
            range.Merge();

           // grid.Cell(currentRow, 11).Text = "起拱度:";
          //  range = grid.Range(currentRow, 12, currentRow, 13);
          //  range.Merge();
            int offset = 0;
            if (gather.MaxTimes.Length > 5)
            {
                offset = 2;
            }
            range = grid.Range(7, 1, currentRow, 12+offset);
            range.set_Borders(FlexCell.EdgeEnum.Outside, FlexCell.LineStyleEnum.Thin);
            range.set_Borders(FlexCell.EdgeEnum.Inside, FlexCell.LineStyleEnum.Thin);
        }

        public void MakeTableButtom(int insertcol)
        {
            FlexCell.Range range;
            grid.Cell(currentRow, 1).Text = "备注:";

            range = grid.Range(currentRow, 2, currentRow, 13);
            range.Merge();

            // grid.Cell(currentRow, 11).Text = "起拱度:";
            //  range = grid.Range(currentRow, 12, currentRow, 13);
            //  range.Merge();
            int offset = 0;
            if (gather.MaxTimes.Length > 5)
            {
                offset = 2;
            }
            range = grid.Range(7, 1, currentRow, 12 + offset + insertcol-1);
            range.set_Borders(FlexCell.EdgeEnum.Outside, FlexCell.LineStyleEnum.Thin);
            range.set_Borders(FlexCell.EdgeEnum.Inside, FlexCell.LineStyleEnum.Thin);
        }
        public void Open(string path)
        {
            this.grid.Controls.Clear();
            this.grid.OpenFile(path);
            currentRow = startRow_2;
        }

        public void SaveAsExel(string filename)
        {
            this.grid.ExportToExcel(filename + ".xls", false, false);
        }
        public void SaveFlex(string filename)
        {
            this.grid.SaveFile(filename + ".flx");
        }

        private void InsertImage(int row, int column, string imagePath, string imageName)
        {
            grid.Images.Add(imagePath, imageName);
            grid.Cell(row, column).SetImage(imageName);
        }
        private void InsertImage(string imagePath, string imageName)
        {
            grid.Images.Add(imagePath, imageName);
            grid.Cell(Image_Rows, Image_Column).SetImage(imageName);
            grid.Cell(Image_Rows, Image_Column).Alignment = AlignmentEnum.CenterGeneral;
        }

        private int GetNumofStations(string beamNum, out JackInfor[] tempjacks)
        {
            string selectString = "select * from Calib where 梁号 = '" + beamNum + "' order by 站号";
            DataTable dt = sql.GetDataTable(selectString);
            if (dt != null)
            {
                tempjacks = new JackInfor[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tempjacks[i].jack_serial = dt.Rows[i]["千斤顶编号"].ToString();
                    tempjacks[i].jack_date = Convert.ToDateTime(dt.Rows[i]["标定日期"].ToString()).ToShortDateString();
                    tempjacks[i].press_sense = dt.Rows[i]["压力传感器"].ToString();
                    //tempjacks[i].jacn_section = dt.Rows[i]["千斤顶断面"].ToString();
                    tempjacks[i].a = Convert.ToDouble(dt.Rows[i]["系数a"]);
                    tempjacks[i].b = Convert.ToDouble(dt.Rows[i]["修正值b"]);
                }
                return dt.Rows.Count;
            }
            tempjacks = null;
            return -1;
        }
        private int GetNumofHoles(string beamNum)
        {
            string selectString = "select * from BeamInformation where 梁号 = '" + beamNum + "'";
            DataTable dt = sql.GetDataTable(selectString);
            if (dt != null)
            {
                return dt.Rows.Count;
            }
            return -1;
        }
        private void GetScaleValue(string beamNum)
        {
            string selectString = "select * from Beams where 梁号 = '" + beamNum + "'";
            DataTable dt = sql.GetDataTable(selectString);
            if (dt != null && dt.Rows.Count > 0)
            {
                firstScale = Convert.ToInt32(dt.Rows[0]["阶段一比例"].ToString());
                secondScale = Convert.ToInt32(dt.Rows[0]["阶段二比例"].ToString());
            }

        }
        private int[] GetWorkStations(int Stations)
        {
            int[] temp;

            switch (Stations)
            {
                case 1234:
                    {
                        temp = new int[4] { 1, 2, 3, 4 };
                        return temp;
                    }
                case 12:
                    {
                        temp = new int[2] { 1, 2 };
                        return temp;
                    }
                case 34:
                    {
                        temp = new int[2] { 3, 4 };
                        return temp;
                    }
                case 1:
                case 2:
                case 3:
                case 4:
                    {
                        temp = new int[1] { Stations };
                        return temp;
                    }
            }
            return null;
        }
        private void MakeTableTop(string beanNum)
        {
            try
            {
                string selectString = "select * from Beams where 梁号 = '" + beanNum + "'";
                DataTable dt = sql.GetDataTable(selectString);
                //  int[] stations = GetWorkStations(Stations);          
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                    {
                        string workMode = dt.Rows[0]["张拉模式"].ToString();

                        grid.Cell(6, 2).Text = dt.Rows[0]["承包单位"].ToString();
                        grid.Cell(6, 6).Text = dt.Rows[0]["监理单位"].ToString();
                        grid.Cell(6, 10).Text = dt.Rows[0]["土建合同号"].ToString();
                        grid.Cell(7, 2).Text = dt.Rows[0]["工程名称"].ToString();
                        grid.Cell(7, 7).Text = dt.Rows[0]["预制梁厂"].ToString();
                        grid.Cell(8, 2).Text = dt.Rows[0]["梁号"].ToString();
                        grid.Cell(8, 7).Text = Convert.ToDateTime(dt.Rows[0]["张拉日期"].ToString()).ToShortDateString();

                        grid.Cell(9, 2).Text = dt.Rows[0]["砼设计强度"].ToString() + "Mpa";
                        grid.Cell(9, 4).Text = dt.Rows[0]["砼实际强度"].ToString() + "Mpa";
                        grid.Cell(9, 7).Text = dt.Rows[0]["弹性模量"].ToString() + "Mpa";
                        grid.Cell(9, 10).Text = dt.Rows[0]["控制应力"].ToString() + "Mpa";

                        string imageName = dt.Rows[0]["图片名称"].ToString();
                        string imagePath = System.Environment.CurrentDirectory;
                        imagePath = imagePath.Insert(imagePath.Length, "\\image\\" + imageName);

                        switch (workMode)
                        {
                            case "单顶单端":
                                {
                                    grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                    grid.Cell(10, 7).Text = jacks[0].jack_date;
                                    grid.Cell(11, 4).Text = "初始行程" + firstScale + "%";
                                    grid.Cell(11, 5).Text = "第一行程" + secondScale + "%";
                                    InsertImage(Image_Rows, Image_Column, imagePath, imageName);
                                    break;
                                }
                            case "双顶双端":
                            case "双顶单端":
                                {
                                    grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                    grid.Cell(10, 7).Text = jacks[0].jack_date;
                                    grid.Cell(11, 2).Text = jacks[1].jack_serial;
                                    grid.Cell(11, 7).Text = jacks[1].jack_date;
                                    grid.Cell(11, 4).Text = "初始行程" + firstScale + "%";
                                    grid.Cell(11, 5).Text = "第一行程" + secondScale + "%";
                                    InsertImage(Image_Rows, Image_Column, imagePath, imageName);

                                    break;
                                }
                            case "四顶双端":
                            case "四顶单端":
                                {
                                    grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                    grid.Cell(10, 7).Text = jacks[0].jack_date;
                                    grid.Cell(11, 2).Text = jacks[1].jack_serial;
                                    grid.Cell(11, 7).Text = jacks[1].jack_date;
                                    grid.Cell(12, 2).Text = jacks[2].jack_serial;
                                    grid.Cell(12, 7).Text = jacks[2].jack_date;
                                    grid.Cell(13, 2).Text = jacks[3].jack_serial;
                                    grid.Cell(13, 7).Text = jacks[3].jack_date;

                                    grid.Cell(14, 4).Text = "初始行程" + firstScale + "%";
                                    grid.Cell(14, 5).Text = "第一行程" + secondScale + "%";

                                    InsertImage(Image_Rows, Image_Column, imagePath, imageName);
                                    break;
                                }
                        }
                    }
                    else
                    {

                        string workMode = dt.Rows[0]["张拉模式"].ToString();

                        grid.Cell(6, 2).Text = dt.Rows[0]["承包单位"].ToString();
                        grid.Cell(6, 8).Text = dt.Rows[0]["监理单位"].ToString();
                        grid.Cell(6, 12).Text = dt.Rows[0]["土建合同号"].ToString();
                        grid.Cell(7, 2).Text = dt.Rows[0]["工程名称"].ToString();
                        grid.Cell(7, 9).Text = dt.Rows[0]["预制梁厂"].ToString();
                        grid.Cell(8, 2).Text = dt.Rows[0]["梁号"].ToString();
                        grid.Cell(8, 9).Text = Convert.ToDateTime(dt.Rows[0]["张拉日期"].ToString()).ToShortDateString();

                        grid.Cell(9, 2).Text = dt.Rows[0]["砼设计强度"].ToString() + "Mpa";
                        grid.Cell(9, 5).Text = dt.Rows[0]["砼实际强度"].ToString() + "Mpa";
                        grid.Cell(9, 9).Text = dt.Rows[0]["弹性模量"].ToString() + "Mpa";
                        grid.Cell(9, 12).Text = dt.Rows[0]["控制应力"].ToString() + "Mpa";

                        string imageName = dt.Rows[0]["图片名称"].ToString();
                        string imagePath = System.Environment.CurrentDirectory;
                        imagePath = imagePath.Insert(imagePath.Length, "\\image\\" + imageName);

                        switch (workMode)
                        {
                            case "单顶单端":
                                {
                                    grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                    grid.Cell(10, 9).Text = jacks[0].jack_date;
                                    grid.Cell(11, 4).Text = "初始行程" + firstScale + "%";
                                    grid.Cell(11, 5).Text = "第一行程" + secondScale + "%";
                                    InsertImage(Image_Rows, Image_Column + 2, imagePath, imageName);
                                    break;
                                }
                            case "双顶双端":
                            case "双顶单端":
                                {
                                    grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                    grid.Cell(10, 9).Text = jacks[0].jack_date;
                                    grid.Cell(11, 2).Text = jacks[1].jack_serial;
                                    grid.Cell(11, 9).Text = jacks[1].jack_date;
                                    grid.Cell(11, 4).Text = "初始行程" + firstScale + "%";
                                    grid.Cell(11, 5).Text = "第一行程" + secondScale + "%";
                                    InsertImage(Image_Rows, Image_Column + 2, imagePath, imageName);

                                    break;
                                }
                            case "四顶双端":
                            case "四顶单端":
                                {
                                    grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                    grid.Cell(10, 9).Text = jacks[0].jack_date;
                                    grid.Cell(11, 2).Text = jacks[1].jack_serial;
                                    grid.Cell(11, 9).Text = jacks[1].jack_date;
                                    grid.Cell(12, 2).Text = jacks[2].jack_serial;
                                    grid.Cell(12, 9).Text = jacks[2].jack_date;
                                    grid.Cell(13, 2).Text = jacks[3].jack_serial;
                                    grid.Cell(13, 9).Text = jacks[3].jack_date;

                                    grid.Cell(14, 4).Text = "初始行程" + firstScale + "%";
                                    grid.Cell(14, 5).Text = "第一行程" + secondScale + "%";

                                    InsertImage(Image_Rows, Image_Column + 2, imagePath, imageName);
                                    break;
                                }
                        }

                    }
                }
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message + "\r\n" + e.ToString());
            }
        }

        public void CreateReport(string beamNum)
        {
            try
            {
                string path = System.Environment.CurrentDirectory;
                path = path.Insert(path.Length, "\\预应力参数.ini");
                Ini ini = new Ini(path);
                this.retraceValue = Convert.ToDouble(ini.ReadValue("预应力参数", "回缩值"));
                this.jack_internal_length = Convert.ToDouble(ini.ReadValue("预应力参数", "顶内伸长"));
            }
            catch (System.FormatException)
            {
            }
            GetScaleValue(beamNum);
            string[] stageTable = new string[] { "初始行程", "第一行程", "第二行程", "第三行程", "第四行程", "第五行程", "第六行程", "第七行程", "第八行程", "第九行程", "第十行程" };
            gather = new GatherData(beamNum, this.sql, firstScale, secondScale);
            GetNumofStations(beamNum, out jacks);
             string path_flex;
             switch (gather.WorkingMode.mode)
             {
                 case "双顶单端":
                     {
                         path_flex = System.Environment.CurrentDirectory;

                         if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_2.flx");
                         }
                         else
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_2_two.flx");
                         }
                         this.Open(path_flex);
                         MakeTableTop(beamNum);
                         currentRow = 13;
                         InsertRecordRows_1();
                         HisData.Data[] max = gather.MaxTimes;                       
                         int InsertCols = max.Length - 5;
                         if (InsertCols < 0) InsertCols = 0;
                         //添加列
                         if (InsertCols > 0)
                         {
                             for (int i = 0; i < max.Length; i++)
                             {
                                 grid.Cell(12, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                             }
                         }
                         else
                         {
                             grid.Cell(12, 4).Text = stageTable[0] + max[0].scale + "%";
                             grid.Cell(12, 5).Text = stageTable[1] + max[1].scale + "%";
                             if (max.Length == 5)
                             {
                                 grid.Cell(12, 6).Text = stageTable[2] + max[2].scale + "%";
                                 grid.Cell(12, 7).Text = stageTable[3] + max[3].scale + "%";
                             }
                         }

                       currentRow = 13;

                         for (int i = 0; i < gather.Datas.Count; i++)
                         {
                             string[] tmpHoles = gather.GetHolesNameArr(gather.holes[i]);
                             grid.Cell(currentRow, 1).Text = tmpHoles[0];
                             grid.Cell(currentRow + 2, 1).Text = tmpHoles[1];

                             grid.Cell(currentRow, 2).Text = (1).ToString(); //站号
                             grid.Cell(currentRow + 2, 2).Text = (2).ToString();
                             for (int j = 0; j < gather.Datas[i].Count; j += 2)
                             {
                                 for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                 {
                                     if (gather.Datas[i][j].hisdata[m].scale == 100)
                                     {
                                         grid.Cell(currentRow, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移   
                                         grid.Cell(currentRow + 2, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 3, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移 

                                     }
                                     else
                                     {
                                         grid.Cell(currentRow, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                         grid.Cell(currentRow + 2, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 3, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移                                      
                                     }
                                 }
                                 grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                 grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j] - (jack_internal_length + retraceValue)).ToString("0.00");
                                 grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                 grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j] - (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");

                                 grid.Cell(currentRow + 2, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                 grid.Cell(currentRow + 2, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j + 1] - (jack_internal_length + retraceValue)).ToString("0.00");
                                 grid.Cell(currentRow + 2, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                 grid.Cell(currentRow + 2, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j + 1] - (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                             }
                             currentRow += 4;
                         }
                         MakeTableButtom();
                         break;
                     }
                 case "单顶单端":
                     {
                         path_flex = System.Environment.CurrentDirectory;
                         if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_1.flx");
                         }
                         else
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_1_two.flx");
                         }
                         this.Open(path_flex);
                       
                         MakeTableTop(beamNum);
                         currentRow = 12;
                         InsertRecordRows_1();

                         HisData.Data[] max = gather.MaxTimes;
                         int InsertCols = max.Length - 5;
                         if (InsertCols < 0) InsertCols = 0;                      
                         //添加列
                         if (InsertCols > 0)
                         {
                             for (int i = 0; i < max.Length; i++)
                             {
                                 grid.Cell(11, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                             }
                         }
                         else
                         {
                             grid.Cell(11, 4).Text = stageTable[0] + max[0].scale + "%";
                             grid.Cell(11, 5).Text = stageTable[1] + max[1].scale + "%";
                             if (max.Length == 5)
                             {
                                 grid.Cell(11, 6).Text = stageTable[2] + max[2].scale + "%";
                                 grid.Cell(11, 7).Text = stageTable[3] + max[3].scale + "%";
                             }
                         }
                         currentRow = 12;
                         for (int i = 0; i < gather.Datas.Count(); i++)
                         {
                             grid.Cell(currentRow, 1).Text = gather.holes[i].holename;
                             for (int j = 0; j < gather.Datas[i].Count; j++)
                             {
                                 grid.Cell(currentRow, 2).Text = (1).ToString();
                                 for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                 {
                                     if (gather.Datas[i][j].hisdata[m].scale == 100)
                                     {
                                         grid.Cell(currentRow, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                     }
                                     else
                                     {
                                         grid.Cell(currentRow, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                     }
                                 }
                                 grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                 grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, 0] - jack_internal_length - retraceValue).ToString("0.00");
                                 grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                 grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, 0] - jack_internal_length - retraceValue) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                             }
                             currentRow += 2;
                         }
                         
                         MakeTableButtom();
                         break;
                     }
                 case "双顶双端":
                     {
                         path_flex = System.Environment.CurrentDirectory;
                         if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length==5)
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_2.flx");
                         }
                         else
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_2_two.flx");
                         }
                       
                         this.Open(path_flex);
                         MakeTableTop(beamNum);
                         InsertRecordRows_2();
                         HisData.Data[] max = gather.MaxTimes;
                         int InsertCols = max.Length - 5;
                         if (InsertCols < 0) InsertCols = 0;
                         //添加列
                         if (InsertCols > 0)
                         {
                             for (int i = 0; i < max.Length; i++)
                             {
                                 grid.Cell(12, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                             }
                         }
                         else
                         {
                             grid.Cell(12, 4).Text = stageTable[0] + max[0].scale + "%";
                             grid.Cell(12, 5).Text = stageTable[1] + max[1].scale + "%";
                             if (max.Length == 5)
                             {
                                 grid.Cell(12, 6).Text = stageTable[2] + max[2].scale + "%";
                                 grid.Cell(12, 7).Text = stageTable[3] + max[3].scale + "%";
                             }
                         }
                         int currentRow = 13;
                         for (int i = 0; i < gather.Datas.Count; i++)
                         {
                             grid.Cell(currentRow, 1).Text = gather.holes[i].holename;
                             for (int j = 0; j < gather.Datas[i].Count; j++)
                             {
                                 grid.Cell(currentRow, 2).Text = (1).ToString(); //站号
                                 grid.Cell(currentRow + j * 2, 2).Text = (2).ToString();

                                 for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                 {
                                     if (gather.Datas[i][j].hisdata[m].scale == 100)
                                     {
                                         grid.Cell(currentRow + 2 * j, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1 + 2 * j, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                     }
                                     else
                                     {
                                         grid.Cell(currentRow + 2 * j, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1 + 2 * j, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                     }
                                 }
                                 grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                 grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, 0] + gather.per_TotalLength[i, 1] - 2 * (jack_internal_length + retraceValue)).ToString("0.00");
                                 grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                 grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, 0] + gather.per_TotalLength[i, 1] - 2 * (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                             }
                             currentRow += 4;
                         }
                    
                         MakeTableButtom();
                         break;
                     }
                 case "四顶双端":
                     {
                         path_flex = System.Environment.CurrentDirectory;
                         if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_4.flx");
                         }
                         else
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_4_two.flx");
                         }
                                                
                         this.Open(path_flex);
                     
                         MakeTableTop(beamNum);
                         currentRow = 15;
                         InsertRecordRows_2();
                      
                         HisData.Data[] max = gather.MaxTimes;
                        
                         int InsertCols = max.Length - 5;
                         if (InsertCols < 0) InsertCols = 0;
                         
                         //添加列
                         if (InsertCols > 0)
                         {
                             for (int i = 0; i < max.Length; i++)
                             {
                                 grid.Cell(14, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                             }
                         }
                         else
                         {
                             grid.Cell(14, 4).Text = stageTable[0] + max[0].scale + "%";
                             grid.Cell(14, 5).Text = stageTable[1] + max[1].scale + "%";
                             if (max.Length == 5)
                             {
                                 grid.Cell(14, 6).Text = stageTable[2] + max[2].scale + "%";
                                 grid.Cell(14, 7).Text = stageTable[3] + max[3].scale + "%";
                             }
                         }
                        currentRow = 15;

                         for (int i = 0; i < gather.Datas.Count; i++)
                         {
                             string[] tmpHoles = gather.GetHolesNameArr(gather.holes[i]);
                             grid.Cell(currentRow, 2).Text = (1).ToString(); //站号
                             grid.Cell(currentRow + 2, 2).Text = (2).ToString();

                             grid.Cell(currentRow + 4, 2).Text = (3).ToString(); //站号
                             grid.Cell(currentRow + 6, 2).Text = (4).ToString();

                             for (int j = 0; j < gather.Datas[i].Count; j += 2)
                             {
                                 grid.Cell(currentRow, 1).Text = tmpHoles[j / 2];

                                 for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                 {
                                     if (gather.Datas[i][j].hisdata[m].scale == 100)
                                     {
                                         grid.Cell(currentRow, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移                                           
                                         grid.Cell(currentRow + 2, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力                                      
                                         grid.Cell(currentRow + 3, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移   
                                     }
                                     else
                                     {
                                         grid.Cell(currentRow, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力                                       
                                         grid.Cell(currentRow + 1, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移                                          
                                         grid.Cell(currentRow + 2, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力                                      
                                         grid.Cell(currentRow + 3, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                     }
                                 }

                                 grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                 grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j] + gather.per_TotalLength[i, j + 1] - 2 * (jack_internal_length + retraceValue)).ToString("0.00");
                                 grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                 grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j] + gather.per_TotalLength[i, j + 1] - 2 * (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                                 currentRow += 4;
                             }
                            // currentRow += 4;
                         }
                         MakeTableButtom();
                         break;
                     }

                 case "四顶单端":
                     {
                         path_flex = System.Environment.CurrentDirectory;
                         if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_4.flx");
                         }
                         else
                         {
                             path_flex = path_flex.Insert(path_flex.Length, "\\model_4_two.flx");
                         }

                         this.Open(path_flex);

                         MakeTableTop(beamNum);
                         currentRow = 15;
                         InsertRecordRows_1();

                         HisData.Data[] max = gather.MaxTimes;

                         int InsertCols = max.Length - 5;
                         if (InsertCols < 0) InsertCols = 0;

                         //添加列
                         if (InsertCols > 0)
                         {
                             for (int i = 0; i < max.Length; i++)
                             {
                                 grid.Cell(14, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                             }
                         }
                         else
                         {
                             grid.Cell(14, 4).Text = stageTable[0] + max[0].scale + "%";
                             grid.Cell(14, 5).Text = stageTable[1] + max[1].scale + "%";
                             if (max.Length == 5)
                             {
                                 grid.Cell(14, 6).Text = stageTable[2] + max[2].scale + "%";
                                 grid.Cell(14, 7).Text = stageTable[3] + max[3].scale + "%";
                             }
                         }
                         currentRow = 15;
                         for (int i = 0; i < gather.Datas.Count; i++)
                         {
                             string[] tmpHoles = gather.GetHolesNameArr(gather.holes[i]);
                             grid.Cell(currentRow, 1).Text = tmpHoles[0];
                             grid.Cell(currentRow + 2, 1).Text = tmpHoles[1];
                             grid.Cell(currentRow + 4, 1).Text = tmpHoles[2];
                             grid.Cell(currentRow + 6, 1).Text = tmpHoles[3];

                             grid.Cell(currentRow, 2).Text = (1).ToString(); //站号
                             grid.Cell(currentRow + 2, 2).Text = (2).ToString();
                             grid.Cell(currentRow + 4, 2).Text = (3).ToString(); //站号
                             grid.Cell(currentRow + 6, 2).Text = (4).ToString();

                             for (int j = 0; j < gather.Datas[i].Count; j += 2)
                             {
                                 for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                 {
                                     if (gather.Datas[i][j].hisdata[m].scale == 100)
                                     {
                                         grid.Cell(currentRow, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                         grid.Cell(currentRow + 2, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 3, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移   
                                     }
                                     else
                                     {
                                         grid.Cell(currentRow, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 1, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                         grid.Cell(currentRow + 2, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                         grid.Cell(currentRow + 3, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                     }
                                 }

                                 grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                 grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j] - (jack_internal_length + retraceValue)).ToString("0.00");
                                 grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                 grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j] - (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");

                                 grid.Cell(currentRow + 2, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                 grid.Cell(currentRow + 2, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j + 1] - (jack_internal_length + retraceValue)).ToString("0.00");
                                 grid.Cell(currentRow + 2, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                 grid.Cell(currentRow + 2, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j + 1] - (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                                 currentRow += 4;
                             }
                             currentRow += 4;
                         }

                         break;
                     }
             }
             SaveFlex(beamNum);
        }
      
        public void CreateReportWithPressure(string beamNum)
        {
            try
            {
                string path = System.Environment.CurrentDirectory;
                path = path.Insert(path.Length, "\\预应力参数.ini");
                Ini ini = new Ini(path);
                this.retraceValue = Convert.ToDouble(ini.ReadValue("预应力参数", "回缩值"));
                this.jack_internal_length = Convert.ToDouble(ini.ReadValue("预应力参数", "顶内伸长"));
            }
            catch (System.FormatException)
            {
            }
            GetScaleValue(beamNum);
            string[] stageTable = new string[] { "初始行程", "第一行程", "第二行程", "第三行程", "第四行程", "第五行程", "第六行程", "第七行程", "第八行程", "第九行程", "第十行程" };
            gather = new GatherData(beamNum, this.sql, firstScale, secondScale);
            GetNumofStations(beamNum, out jacks);
            string path_flex;
            switch (gather.WorkingMode.mode)
            {

                #region 双顶单端
                case "双顶单端":
                    {
                        path_flex = System.Environment.CurrentDirectory;

                        if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_2.flx");
                        }
                        else
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_2_two.flx");
                        }
                        this.Open(path_flex);
                        MakeTableTop(beamNum);
                        currentRow = 13;
                        InsertRecordRows_1_pressure();
                        HisData.Data[] max = gather.MaxTimes;
                        int InsertCols = max.Length - 5;
                        if (InsertCols < 0) InsertCols = 0;
                        //添加列
                        if (InsertCols > 0)
                        {
                            grid.InsertCol(4, InsertCols - 2);
                            for (int i = 0; i < max.Length; i++)
                            {
                                grid.Cell(12, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                            }
                        }
                        else
                        {
                            grid.Cell(12, 4).Text = stageTable[0] + max[0].scale + "%";
                            grid.Cell(12, 5).Text = stageTable[1] + max[1].scale + "%";
                            if (max.Length == 5)
                            {
                                grid.Cell(12, 6).Text = stageTable[2] + max[2].scale + "%";
                                grid.Cell(12, 7).Text = stageTable[3] + max[3].scale + "%";
                            }
                        }

                        currentRow = 13;

                        for (int i = 0; i < gather.Datas.Count; i++)
                        {
                            string[] tmpHoles = gather.GetHolesNameArr(gather.holes[i]);
                            grid.Cell(currentRow, 1).Text = tmpHoles[0];
                            grid.Cell(currentRow + 3, 1).Text = tmpHoles[1];

                            grid.Cell(currentRow, 2).Text = (1).ToString(); //站号
                            grid.Cell(currentRow + 3, 2).Text = (2).ToString();
                        
                            for (int j = 0; j < gather.Datas[i].Count; j += 2)
                            {
                                for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                {
                                    if (gather.Datas[i][j].hisdata[m].scale == 100)
                                    {
                                        try{
                                        grid.Cell(currentRow, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                        grid.Cell(currentRow+1, Col_100 + InsertCols).Text = (gather.Datas[i][j].hisdata[m].power*jacks[j].a+jacks[j].b).ToString("0.00");
                                        grid.Cell(currentRow + 2, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移   

                                        grid.Cell(currentRow + 3, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                        grid.Cell(currentRow + 4, Col_100 + InsertCols).Text = (gather.Datas[i][j + 1].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00");//初始行程 力
                                        grid.Cell(currentRow + 5, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移 
                                        }
                                        catch(ArgumentOutOfRangeException){
                                        }
                                    }
                                    else
                                    {
                                        try{
                                        grid.Cell(currentRow, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                        grid.Cell(currentRow + 1, 4 + m).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00"); //初始行程 力
                                        grid.Cell(currentRow + 2, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                       
                                        grid.Cell(currentRow + 3, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                        grid.Cell(currentRow + 4, 4 + m).Text = (gather.Datas[i][j + 1].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00");//初始行程 力
                                        grid.Cell(currentRow + 5, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移                                      
                                        }
                                        catch(ArgumentOutOfRangeException){
                                        }
                                        }

                                }
                                grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j] - (jack_internal_length + retraceValue)).ToString("0.00");
                                grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j] - (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");

                                grid.Cell(currentRow + 3, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                grid.Cell(currentRow + 3, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j + 1] - (jack_internal_length + retraceValue)).ToString("0.00");
                                grid.Cell(currentRow + 3, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                grid.Cell(currentRow + 3, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j + 1] - (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                            }
                            currentRow += 6;
                        }
                        MakeTableButtom();
                        break;
                    }
                #endregion
                #region 单顶单端
                case "单顶单端":
                    {
                        path_flex = System.Environment.CurrentDirectory;
                        if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_1.flx");
                        }
                        else
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_1_two.flx");
                        }
                        this.Open(path_flex);

                        MakeTableTop(beamNum);
                        currentRow = 12;
                        InsertRecordRows_1_pressure();

                        HisData.Data[] max = gather.MaxTimes;
                        int InsertCols = max.Length - 5;
                        if (InsertCols < 0) InsertCols = 0;
                        //添加列
                        if (InsertCols > 0)
                        {
                            grid.InsertCol(4, InsertCols - 2);
                            for (int i = 0; i < max.Length; i++)
                            {
                                grid.Cell(11, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                            }
                        }
                        else
                        {
                            grid.Cell(11, 4).Text = stageTable[0] + max[0].scale + "%";
                            grid.Cell(11, 5).Text = stageTable[1] + max[1].scale + "%";
                            if (max.Length == 5)
                            {
                                grid.Cell(11, 6).Text = stageTable[2] + max[2].scale + "%";
                                grid.Cell(11, 7).Text = stageTable[3] + max[3].scale + "%";
                            }
                        }
                        currentRow = 12;
                        for (int i = 0; i < gather.Datas.Count(); i++)
                        {
                            grid.Cell(currentRow, 1).Text = gather.holes[i].holename;
                            for (int j = 0; j < gather.Datas[i].Count; j++)
                            {
                                grid.Cell(currentRow, 2).Text = (1).ToString();
                                for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                {
                                    if (gather.Datas[i][j].hisdata[m].scale == 100)
                                    {
                                        try
                                        {
                                            grid.Cell(currentRow, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 1, Col_100 + InsertCols).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 2, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                        }
                                    else
                                    {
                                        try
                                        {
                                            grid.Cell(currentRow, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 1, 4 + m).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00");//初始行程 力
                                            grid.Cell(currentRow + 2, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                        }
                                }
                                grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, 0] - jack_internal_length - retraceValue).ToString("0.00");
                                grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, 0] - jack_internal_length - retraceValue) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                            }
                            currentRow += 3;
                        }

                        MakeTableButtom();
                        break;
                    }
                #endregion
                #region 双顶双端
                case "双顶双端":
                    {
                        path_flex = System.Environment.CurrentDirectory;
                        if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_2.flx");
                        }
                        else
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_2_two.flx");
                        }

                        this.Open(path_flex);
                        MakeTableTop(beamNum);
                        InsertRecordRows_2_pressure();
                        HisData.Data[] max = gather.MaxTimes;
                        int InsertCols = max.Length - 5;
                        if (InsertCols < 0) InsertCols = 0;
                        //添加列
                        if (InsertCols > 0)
                        {
                            grid.InsertCol(4, InsertCols-2);
                            for (int i = 0; i < max.Length; i++)
                            {
                                
                                grid.Cell(12, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                            }
                        }
                        else
                        {
                            grid.Cell(12, 4).Text = stageTable[0] + max[0].scale + "%";
                            grid.Cell(12, 5).Text = stageTable[1] + max[1].scale + "%";
                            if (max.Length == 5)
                            {
                                grid.Cell(12, 6).Text = stageTable[2] + max[2].scale + "%";
                                grid.Cell(12, 7).Text = stageTable[3] + max[3].scale + "%";
                            }
                        }
                        int currentRow = 13;
                        for (int i = 0; i < gather.Datas.Count; i++)
                        {
                            grid.Cell(currentRow, 1).Text = gather.holes[i].holename;
                            for (int j = 0; j < gather.Datas[i].Count; j++)
                            {
                                grid.Cell(currentRow, 2).Text = (1).ToString(); //站号
                                grid.Cell(currentRow +1+ j * 2, 2).Text = (2).ToString();

                                for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                {
                                    if (gather.Datas[i][j].hisdata[m].scale == 100)
                                    {
                                        try
                                        {
                                            grid.Cell(currentRow + 3 * j, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); // 力
                                            grid.Cell(currentRow + 1 + 3 * j, Col_100 + InsertCols).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00");//压强  
                                            grid.Cell(currentRow + 2 + 3 * j, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");// 位移    
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            grid.Cell(currentRow + 3 * j, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); // 力
                                            grid.Cell(currentRow + 1 + 3 * j, 4 + m).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00");// 压强  
                                            grid.Cell(currentRow + 2 + 3 * j, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");// 位移    
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                        }
                                }
                                grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, 0] + gather.per_TotalLength[i, 1] - 2 * (jack_internal_length + retraceValue)).ToString("0.00");
                                grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, 0] + gather.per_TotalLength[i, 1] - 2 * (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                            }
                            currentRow += 6;
                        }

                        MakeTableButtom(InsertCols);
                        break;
                    }
                #endregion
                #region 四顶双端
                case "四顶双端":
                    {
                        path_flex = System.Environment.CurrentDirectory;
                        if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_4.flx");
                        }
                        else
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_4_two.flx");
                        }

                        this.Open(path_flex);

                        MakeTableTop(beamNum);
                        currentRow = 15;
                        InsertRecordRows_2_pressure();

                        HisData.Data[] max = gather.MaxTimes;

                        int InsertCols = max.Length - 5;
                        if (InsertCols < 0) InsertCols = 0;

                        //添加列
                        if (InsertCols > 0)
                        {
                            grid.InsertCol(4, InsertCols - 2);
                            for (int i = 0; i < max.Length; i++)
                            {
                                grid.Cell(14, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                            }
                        }
                        else
                        {
                            grid.Cell(14, 4).Text = stageTable[0] + max[0].scale + "%";
                            grid.Cell(14, 5).Text = stageTable[1] + max[1].scale + "%";
                            if (max.Length == 5)
                            {
                                grid.Cell(14, 6).Text = stageTable[2] + max[2].scale + "%";
                                grid.Cell(14, 7).Text = stageTable[3] + max[3].scale + "%";
                            }
                        }
                        currentRow = 15;

                        for (int i = 0; i < gather.Datas.Count; i++)
                        {
                            string[] tmpHoles = gather.GetHolesNameArr(gather.holes[i]);
                            grid.Cell(currentRow, 2).Text = (1).ToString(); //站号
                            grid.Cell(currentRow + 3, 2).Text = (2).ToString();

                            grid.Cell(currentRow + 6, 2).Text = (3).ToString(); //站号
                            grid.Cell(currentRow + 9, 2).Text = (4).ToString();

                            for (int j = 0; j < gather.Datas[i].Count; j += 2)
                            {
                                grid.Cell(currentRow, 1).Text = tmpHoles[j / 2];

                                for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                {
                                    if (gather.Datas[i][j].hisdata[m].scale == 100)
                                    {
                                        try
                                        {
                                            double power_1 = gather.Datas[i][j].hisdata[m].power+10;

                                            grid.Cell(currentRow, Col_100 + InsertCols).Text = power_1.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 1, Col_100 + InsertCols).Text = (power_1 * jacks[j].a + jacks[j].b).ToString("0.00");//初始行程 压强
                                            //grid.Cell(currentRow, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            //grid.Cell(currentRow + 1, Col_100 + InsertCols).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00");//初始行程 压强
                                            grid.Cell(currentRow + 2, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    

                                            double power_2 = gather.Datas[i][j + 1].hisdata[m].power + 10;
                                            if ((j + 1) == 3) power_2 -= 10;
                                            grid.Cell(currentRow + 3, Col_100 + InsertCols).Text = power_2.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 4, Col_100 + InsertCols).Text = (power_2 * jacks[j + 1].a + jacks[j + 1].b).ToString("0.00");
                                            //grid.Cell(currentRow + 3, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            //grid.Cell(currentRow + 4, Col_100 + InsertCols).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j + 1].a + jacks[j + 1].b).ToString("0.00");
                                            grid.Cell(currentRow + 5, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移   
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                       }
                                    else
                                    {
                                        try
                                        {
                                            grid.Cell(currentRow, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 1, 4 + m).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 2, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    

                                            grid.Cell(currentRow + 3, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 4, 4 + m).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j + 1].a + jacks[j + 1].b).ToString("0.00");  //初始行程 力
                                            grid.Cell(currentRow + 5, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移  
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                        
                                    }
                                }

                                grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j] + gather.per_TotalLength[i, j + 1] - 2 * (jack_internal_length + retraceValue)).ToString("0.00");
                                grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j] + gather.per_TotalLength[i, j + 1] - 2 * (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                                currentRow += 6;
                            }
                            // currentRow += 4;
                        }
                        MakeTableButtom();
                        break;
                    }
                #endregion
                #region 四顶单端
                case "四顶单端":
                    {
                        path_flex = System.Environment.CurrentDirectory;
                        if (gather.MaxTimes.Length == 3 || gather.MaxTimes.Length == 5)
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_4.flx");
                        }
                        else
                        {
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_4_two.flx");
                        }

                        this.Open(path_flex);

                        MakeTableTop(beamNum);
                        currentRow = 15;
                        InsertRecordRows_1_pressure();

                        HisData.Data[] max = gather.MaxTimes;

                        int InsertCols = max.Length - 5;
                        if (InsertCols < 0) InsertCols = 0;

                        //添加列
                        if (InsertCols > 0)
                        {
                            grid.InsertCol(4, InsertCols - 2);
                            for (int i = 0; i < max.Length; i++)
                            {
                                grid.Cell(14, 4 + i).Text = stageTable[i] + max[i].scale + "%";
                            }
                        }
                        else
                        {
                            grid.Cell(14, 4).Text = stageTable[0] + max[0].scale + "%";
                            grid.Cell(14, 5).Text = stageTable[1] + max[1].scale + "%";
                            if (max.Length == 5)
                            {
                                grid.Cell(14, 6).Text = stageTable[2] + max[2].scale + "%";
                                grid.Cell(14, 7).Text = stageTable[3] + max[3].scale + "%";
                            }
                        }
                        currentRow = 15;
                        for (int i = 0; i < gather.Datas.Count; i++)
                        {
                            string[] tmpHoles = gather.GetHolesNameArr(gather.holes[i]);
                            grid.Cell(currentRow, 1).Text = tmpHoles[0];
                            grid.Cell(currentRow + 3, 1).Text = tmpHoles[1];
                            grid.Cell(currentRow + 6, 1).Text = tmpHoles[2];
                            grid.Cell(currentRow + 9, 1).Text = tmpHoles[3];

                            grid.Cell(currentRow, 2).Text = (1).ToString(); //站号
                            grid.Cell(currentRow + 3, 2).Text = (2).ToString();
                            grid.Cell(currentRow + 6, 2).Text = (3).ToString(); //站号
                            grid.Cell(currentRow + 9, 2).Text = (4).ToString();

                            for (int j = 0; j < gather.Datas[i].Count; j += 2)
                            {
                                for (int m = 0; m < gather.Datas[i][j].hisdata.Count; m++)
                                {
                                    if (gather.Datas[i][j].hisdata[m].scale == 100)
                                    {
                                        try
                                        {
                                            grid.Cell(currentRow, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 1, Col_100 + InsertCols).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 2, Col_100 + InsertCols).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    

                                            grid.Cell(currentRow + 3, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 4, Col_100 + InsertCols).Text = (gather.Datas[i][j + 1].hisdata[m].power * jacks[j + 1].a + jacks[j + 1].b).ToString("0.00");  //初始行程 力
                                            grid.Cell(currentRow + 5, Col_100 + InsertCols).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移   
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            grid.Cell(currentRow, 4 + m).Text = gather.Datas[i][j].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 1, 4 + m).Text = (gather.Datas[i][j].hisdata[m].power * jacks[j].a + jacks[j].b).ToString("0.00");//初始行程 力
                                            grid.Cell(currentRow + 2, 4 + m).Text = gather.Datas[i][j].hisdata[m].length.ToString("0.00");//初始行程 位移    

                                            grid.Cell(currentRow + 3, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].power.ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 4, 4 + m).Text = (gather.Datas[i][j + 1].hisdata[m].power * jacks[j + 1].a + jacks[j + 1].b).ToString("0.00"); //初始行程 力
                                            grid.Cell(currentRow + 5, 4 + m).Text = gather.Datas[i][j + 1].hisdata[m].length.ToString("0.00");//初始行程 位移    
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                        }
                                }

                                grid.Cell(currentRow, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                grid.Cell(currentRow, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j] - (jack_internal_length + retraceValue)).ToString("0.00");
                                grid.Cell(currentRow, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                grid.Cell(currentRow, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j] - (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");

                                grid.Cell(currentRow + 3, Col_100 + InsertCols + 1).Text = gather.holes[i].D_Power.ToString("0.00");//设计力   
                                grid.Cell(currentRow + 3, Col_100 + InsertCols + 2).Text = (gather.per_TotalLength[i, j + 1] - (jack_internal_length + retraceValue)).ToString("0.00");
                                grid.Cell(currentRow + 3, Col_100 + InsertCols + 3).Text = gather.holes[i].D_Length.ToString("0.00");//设计伸长
                                grid.Cell(currentRow + 3, Col_100 + InsertCols + 4).Text = (((gather.per_TotalLength[i, j + 1] - (jack_internal_length + retraceValue)) / gather.holes[i].D_Length - 1) * 100.0).ToString("0.00");
                                currentRow += 6;
                            }
                            currentRow += 6;
                        }
                        break;
                    }
                #endregion
            }
            SaveFlex(beamNum);
        }
    }
}
