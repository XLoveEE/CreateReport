﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace RemoteDB_DLL
{
    public class RemoteDB
    {
       public   struct beamInfor
        {
            public string beanname;
            public string beammode;
            public string holename;
            public string designlength;
            public string designstress;
            public DateTime stressdate;
            public string designintensity;
            public float realintensity;
            public int percent1;
            public int percent2;
            public string imagename;
            public string DeviceID;
        }

      public   struct RemoteData
        {
            public string beamname;
            public int stationno;
            public int times;
            public float stress;
            public float length;
            public int stage;
            public float time;
            public string DeviceID;
        }
       
        public OleDbConnection olecon = new OleDbConnection();
        public bool IsOpen=false;

        public void Open()
        {
            olecon.ConnectionString = "Provider=SQLOLEDB;Data Source=122.114.122.10;Persist Security Info=True;User ID=znzlzy;Initial Catalog=znzlzy;pwd=onlyfortest";
            olecon.Open();
            if (this.olecon.State == System.Data.ConnectionState.Open)
            {
                this.IsOpen = true;
            }
        }
        public void Close()
        {
            if (this.olecon.State == System.Data.ConnectionState.Open)
            {
                this.olecon.Close();
                this.IsOpen = false;
            }
        }

        public bool  InsertBeamInfor(beamInfor tmpBeam)
        {
            
            string insertStr = "insert into beam (beamname,beammode,holename,designlength,designstress,stressdate,designintensity,realintensity,percent1,percent2,imagename,DeviceID) values('"+
                tmpBeam.beanname+"','"+tmpBeam.beammode+"','"+tmpBeam.holename+"','"+tmpBeam.designlength+"','"+tmpBeam.designstress+"','"+tmpBeam.stressdate+"','"+tmpBeam.designintensity+"','"+
                    tmpBeam.realintensity+"',"+tmpBeam.percent1+","+tmpBeam.percent2+",'"+tmpBeam.imagename+"','"+tmpBeam.DeviceID+"')";
            OleDbCommand cmd = new OleDbCommand(insertStr, this.olecon);
             int res = cmd.ExecuteNonQuery();
             if (res > 0)
             {
                 return true;
             }
             else
             {
                 return false;
             }
        }

        public bool InsertRemoteData(RemoteData tmpData)
        {
            string insertStr = "insert into data (beamname,stationno,times,stress,length,stage,time,DeviceID) values ('" + tmpData.beamname + "'," +
                tmpData.stationno + "," + tmpData.times + "," + tmpData.stress + "," + tmpData.length + "," + tmpData.stage + "," + tmpData.time + ",'" + tmpData.DeviceID + "')";
            OleDbCommand cmd = new OleDbCommand(insertStr, this.olecon);
            int res = cmd.ExecuteNonQuery();
            if (res > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteRemoteBeamInfro(beamInfor tmpBeam)
        {
            string deleteStr = "delete from beam where beamname='" + tmpBeam.beanname + "' and DeviceID = '" + tmpBeam.DeviceID + "'";
            OleDbCommand cmd = new OleDbCommand(deleteStr, this.olecon);
            int res = cmd.ExecuteNonQuery();
            if (res > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
          
        }

        public bool DeleteRemoteDate(string beamname,string DeviceID)
        {
            string deleteStr = "delete from data where beamname='" + beamname + "' and DeviceID = '" + DeviceID + "'";
            OleDbCommand cmd = new OleDbCommand(deleteStr, this.olecon);
            int res = cmd.ExecuteNonQuery();
            if (res > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
