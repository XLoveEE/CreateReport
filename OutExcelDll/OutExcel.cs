﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlServerCe;
using FlexCell;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using Ini_Dll;
using GatherDataDll;
using SqlDBOperationDll;
namespace OutExcelDll
{
    public class OutExcel
    {
        struct JackInfor
        {
            public string jack_serial;
            public string jack_date;
            public string press_sense;
            public string jacn_section;
        }

        public  SqlCeConnection sqlcecon = new SqlCeConnection();
       
        private string db_Path;
        private FlexCell.Grid grid = new FlexCell.Grid();
        private const int startRow_2 = 13;
        private const int Image_Rows = 8;
        private const int Image_Column = 11;
        private int currentRow;
        JackInfor[] jacks;
        private double retraceValue;
        private double jack_internal_length;
        private int firstScale;
        private int secondScale;
        GatherData gather;

        public string DB_Path
        {
            get { return db_Path; }
        }
        public OutExcel()
        {
           
        }
        public DataTable GetDataTable(string selectString)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCeDataAdapter adapter = new SqlCeDataAdapter(selectString, sqlcecon);
                SqlCeCommandBuilder scb = new SqlCeCommandBuilder(adapter);
                adapter.Fill(dt);
                return dt;
            }
            catch (SqlCeException)
            {
                return null;
            }
            catch (ArgumentException)
            {
                return null;
            }
        }
        public string GetDataBasePath()
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "sdf文件|*.sdf";
            if (open.ShowDialog() == DialogResult.OK)
            {
                db_Path = open.FileName;
                return db_Path;
            }
            else
            {
                return null;
            }
            
        }
        public bool ConnectDataBase()
        {
            string dbPath = "Data Source = " + this.db_Path+";Password=zzjnzlsj;";
            sqlcecon.ConnectionString = dbPath;
            sqlcecon.Open();
            if (sqlcecon.State == System.Data.ConnectionState.Open)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CloseConnectDataBase()
        {
            if (sqlcecon.State == System.Data.ConnectionState.Open)
            {
                sqlcecon.Close();
                return true;
            }
            else
            {
                return false;
            }
        }
        public DataTable GetBeamNums(DateTime startTime , DateTime endTime)
        {
            string selectString = "select 梁号 from Beams where 张拉日期 between '" + startTime.AddDays(-1).ToShortDateString() + "' and '" + endTime.AddDays(1).ToShortDateString() + "'";
            return GetDataTable(selectString);
        }

        public void InsertRecordRows_2(List<List<ExcelData>> tempExcelData)
        {
            int row = tempExcelData.Count;
            int column = tempExcelData[0].Count;
            for (int i = 0; i < row; i++)
            {
                string[] holes = GetHoleNames(tempExcelData[i][0].holeName);
                for (int j = 0; j < column; j+=2)
                {
                    FlexCell.Range range;
                    //钢束编号
                    range = grid.Range(currentRow, 1, currentRow + 3, 1);
                    range.Merge();
                    grid.Cell(currentRow, 1).Text = tempExcelData[i][0].holeName;
                    //张拉断面

                    range = grid.Range(currentRow, 2, currentRow + 1, 2);
                    range.Merge();

                    range = grid.Range(currentRow + 2, 2, currentRow + 3, 2);
                    range.Merge();

                    grid.Cell(currentRow, 2).Text = "张拉仪" +tempExcelData[i][j].StationNum;
                    grid.Cell(currentRow + 2, 2).Text = "张拉仪" + tempExcelData[i][j+1].StationNum;

                    grid.Cell(currentRow, 3).Text = "张拉力(KN)";
                    grid.Cell(currentRow + 1, 3).Text = "伸长量(mm)";
                    grid.Cell(currentRow + 2, 3).Text = "张拉力(KN)";
                    grid.Cell(currentRow + 3, 3).Text = "伸长量(mm)";

                    grid.Cell(currentRow, 4).Text = tempExcelData[i][j].firstscale_power.ToString("0.00");
                    grid.Cell(currentRow + 1, 4).Text = tempExcelData[i][j].firstscale_length.ToString("0.00");
                    grid.Cell(currentRow + 2, 4).Text = tempExcelData[i][j+1].firstscale_power.ToString("0.00");
                    grid.Cell(currentRow + 3, 4).Text = tempExcelData[i][j+1].firstscale_length.ToString("0.00");

                    grid.Cell(currentRow, 5).Text = tempExcelData[i][j].secondscale_power.ToString("0.00");
                    grid.Cell(currentRow + 1, 5).Text = tempExcelData[i][j].secondscale_length.ToString("0.00");
                    grid.Cell(currentRow + 2, 5).Text = tempExcelData[i][j+1].secondscale_power.ToString("0.00");
                    grid.Cell(currentRow + 3, 5).Text = tempExcelData[i][j+1].secondscale_length.ToString("0.00");

                    if (tempExcelData[i][0].fiftyscale_2_power > 0)
                    {
                        grid.Cell(currentRow, 6).Text = tempExcelData[i][j].fiftyscale_1_power.ToString("0.00");
                        grid.Cell(currentRow + 1, 6).Text = tempExcelData[i][j].fiftyscale_1_length.ToString("0.00");
                        grid.Cell(currentRow + 2, 6).Text = tempExcelData[i][j+1].fiftyscale_1_power.ToString("0.00");
                        grid.Cell(currentRow + 3, 6).Text = tempExcelData[i][1].fiftyscale_1_length.ToString("0.00");

                        grid.Cell(currentRow, 7).Text = tempExcelData[i][j].fiftyscale_2_power.ToString("0.00");
                        grid.Cell(currentRow + 1, 7).Text = tempExcelData[i][j].fiftyscale_2_length.ToString("0.00");
                        grid.Cell(currentRow + 2, 7).Text = tempExcelData[i][j+1].fiftyscale_2_power.ToString("0.00");
                        grid.Cell(currentRow + 3, 7).Text = tempExcelData[i][j+1].fiftyscale_2_length.ToString("0.00");
                    }

                    grid.Cell(currentRow, 8).Text = tempExcelData[i][j].hundredscale_power.ToString("0.00");
                    grid.Cell(currentRow + 1, 8).Text = tempExcelData[i][j].hundredscale_length.ToString("0.00");
                    grid.Cell(currentRow + 2, 8).Text = tempExcelData[i][j+1].hundredscale_power.ToString("0.00");
                    grid.Cell(currentRow + 3, 8).Text = tempExcelData[i][j+1].hundredscale_length.ToString("0.00");


                    //设计张拉力
                    range = grid.Range(currentRow, 9, currentRow + 3, 9);
                    range.Merge();
                    grid.Cell(currentRow, 9).Text = tempExcelData[i][j].D_power.ToString("0.00");
                    //range = grid.Range(currentRow + 2, 9, currentRow + 3, 9);
                    //range.Merge();
                    //总伸长量
                    range = grid.Range(currentRow, 10, currentRow + 3, 10);
                    range.Merge();
                    grid.Cell(currentRow, 10).Text = (tempExcelData[i][j].TotalLength + tempExcelData[i][j+1].TotalLength - 2 * (retraceValue + jack_internal_length)).ToString("0.00");
                    //理论伸长量             
                    range = grid.Range(currentRow, 11, currentRow + 3, 11);
                    range.Merge();
                    grid.Cell(currentRow, 11).Text = tempExcelData[i][j].D_length.ToString("0.00");

                    //延伸量误差
                    range = grid.Range(currentRow, 12, currentRow + 3, 12);
                    range.Merge();
                    grid.Cell(currentRow, 12).Text = ((((tempExcelData[i][j].TotalLength + tempExcelData[i][j+1].TotalLength - 2 * (retraceValue + jack_internal_length)) - tempExcelData[i][j].D_length) / tempExcelData[i][j].D_length) * 100.0).ToString("0.00");
                    //断丝处理
                    range = grid.Range(currentRow, 13, currentRow + 3, 13);
                    range.Merge();
                    range = grid.Range(currentRow, 1, currentRow + 3, 13);
                    currentRow += 4;
                }
            }
        }
        public void InsertRecordRows_1(List<List<ExcelData>> tempExcelData)
        {
           
            int row = tempExcelData.Count;
            int column = tempExcelData[0].Count;
          
            for (int i = 0; i < row; i++)
            {
                string[] holes = GetHoleNames(tempExcelData[i][0].holeName);
                for (int j = 0; j < column; j++)
                {
                    FlexCell.Range range;
                    //钢束编号
                    range = grid.Range(currentRow, 1, currentRow + 1, 1);
                    range.Merge();
                    grid.Cell(currentRow, 1).Text = holes[j];
                    //张拉断面
                    range = grid.Range(currentRow, 2, currentRow + 1, 2);
                    range.Merge();
                  
                    //range = grid.Range(currentRow + 2, 2, currentRow + 1, 2);
                    //range.Merge();

                    grid.Cell(currentRow, 2).Text = "张拉仪" + tempExcelData[i][j].StationNum;

                    grid.Cell(currentRow, 3).Text = "张拉力(KN)";
                    grid.Cell(currentRow + 1, 3).Text = "伸长量(mm)";

                    grid.Cell(currentRow, 4).Text = tempExcelData[i][j].firstscale_power.ToString("0.00");
                    grid.Cell(currentRow + 1, 4).Text = tempExcelData[i][j].firstscale_length.ToString("0.00");

                    grid.Cell(currentRow, 5).Text = tempExcelData[i][j].secondscale_power.ToString("0.00");
                    grid.Cell(currentRow + 1, 5).Text = tempExcelData[i][j].secondscale_length.ToString("0.00");

                    if (tempExcelData[i][0].fiftyscale_2_power > 0)
                    {
                        grid.Cell(currentRow, 6).Text = tempExcelData[i][j].fiftyscale_1_power.ToString("0.00");
                        grid.Cell(currentRow + 1, 6).Text = tempExcelData[i][j].fiftyscale_1_length.ToString("0.00");

                        grid.Cell(currentRow, 7).Text = tempExcelData[i][j].fiftyscale_2_power.ToString("0.00");
                        grid.Cell(currentRow + 1, 7).Text = tempExcelData[i][j].fiftyscale_2_length.ToString("0.00");
                    }

                    grid.Cell(currentRow, 8).Text = tempExcelData[i][j].hundredscale_power.ToString("0.00");
                    grid.Cell(currentRow + 1, 8).Text = tempExcelData[i][j].hundredscale_length.ToString("0.00");

                    //设计张拉力
                    range = grid.Range(currentRow, 9, currentRow + 1, 9);
                    range.Merge();
                    grid.Cell(currentRow, 9).Text = tempExcelData[i][j].D_power.ToString("0.00");
                    //range = grid.Range(currentRow + 2, 9, currentRow + 3, 9);
                    //range.Merge();
                    //总伸长量
                    range = grid.Range(currentRow, 10, currentRow + 1, 10);
                    range.Merge();
                    grid.Cell(currentRow, 10).Text = (tempExcelData[i][j].TotalLength - (retraceValue + jack_internal_length)).ToString("0.00");
               
                    //理论伸长量             
                    range = grid.Range(currentRow, 11, currentRow + 1, 11);
                    range.Merge();
                    grid.Cell(currentRow, 11).Text = tempExcelData[i][j].D_length.ToString("0.00");

                    //延伸量误差
                    range = grid.Range(currentRow, 12, currentRow + 1, 12);
                    range.Merge();
                    grid.Cell(currentRow, 12).Text = ((((tempExcelData[i][j].TotalLength-(retraceValue + jack_internal_length)) - tempExcelData[i][j].D_length) / tempExcelData[i][j].D_length) * 100.0).ToString("0.00");
                    //断丝处理
                    range = grid.Range(currentRow, 13, currentRow + 1, 13);
                    range.Merge();
                    range = grid.Range(currentRow, 1, currentRow + 1, 13);
                    currentRow += 2;
                }
            }
        }
        
        public void MakeTableButtom()
        {
            FlexCell.Range range;
            grid.Cell(currentRow, 1).Text = "备注:";

            range = grid.Range(currentRow, 2, currentRow, 10);
            range.Merge();

            grid.Cell(currentRow, 11).Text = "起拱度:";
            range = grid.Range(currentRow, 12, currentRow, 13);
            range.Merge();

            range = grid.Range(7, 1, currentRow - 1, 12);
            range.set_Borders(FlexCell.EdgeEnum.Outside, FlexCell.LineStyleEnum.Thin);
            range.set_Borders(FlexCell.EdgeEnum.Inside, FlexCell.LineStyleEnum.Thin);
        }
        public void Open(string path)
        {
            this.grid.Controls.Clear();
            this.grid.OpenFile(path);
            currentRow = startRow_2;
        }

        public void SaveAsExel(string filename)
        {
            this.grid.ExportToExcel(filename + ".xls", false, false);
        }
        public void SaveFlex(string filename)
        {
            this.grid.SaveFile(filename + ".flx");
        }
        private void InsertImage(int row, int column, string imagePath, string imageName)
        {
            grid.Images.Add(imagePath, imageName);
            grid.Cell(row, column).SetImage(imageName);
        }
        private void InsertImage(string imagePath, string imageName)
        {
            grid.Images.Add(imagePath, imageName);
            grid.Cell(Image_Rows, Image_Column).SetImage(imageName);
            grid.Cell(Image_Rows, Image_Column).Alignment = AlignmentEnum.CenterGeneral;
        }
        private int GetNumofStations(string beamNum, out JackInfor[] tempjacks)
        {
            string selectString = "select * from Calib where 梁号 = '" + beamNum + "'";
            DataTable dt = this.GetDataTable(selectString);
            if (dt != null)
            {
                tempjacks = new JackInfor[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tempjacks[i].jack_serial = dt.Rows[i]["千斤顶编号"].ToString();
                    tempjacks[i].jack_date = Convert.ToDateTime(dt.Rows[i]["标定日期"].ToString()).ToShortDateString();
                    tempjacks[i].press_sense = dt.Rows[i]["压力传感器"].ToString();
                    //tempjacks[i].jacn_section = dt.Rows[i]["千斤顶断面"].ToString();
                }
                return dt.Rows.Count;
            }
            tempjacks = null;
            return -1;
        }
        private int GetNumofHoles(string beamNum)
        {
            string selectString = "select * from BeamInformation where 梁号 = '" + beamNum + "'";
            DataTable dt = this.GetDataTable(selectString);
            if (dt != null)
            {
                return dt.Rows.Count;
            }
            return -1;
        }
        private void GetScaleValue(string beamNum)
        {
            string selectString = "select * from Beams where 梁号 = '" + beamNum + "'";
            DataTable dt = this.GetDataTable(selectString);
            if (dt != null && dt.Rows.Count > 0)
            {
                firstScale = Convert.ToInt32(dt.Rows[0]["阶段一比例"].ToString());
                secondScale = Convert.ToInt32(dt.Rows[0]["阶段二比例"].ToString());
            }
           
        }
           
        private string[] GetHoleNames(string tempHoles)
        {
            List<string> tempHole = new List<string>();
            int index = 0;
            do
            {
                try
                {
                    index = tempHoles.IndexOf(":");
                    tempHole.Add(tempHoles.Substring(0, index));
                    tempHoles = tempHoles.Substring(index + 1);
                }
                catch (System.ArgumentOutOfRangeException)
                {
                }
                catch(NullReferenceException)
                {

                }
            } while (index > 0);
            tempHole.Add(tempHoles);

            return tempHole.ToArray();
        }     
        private void MakeTableTop(string beanNum)
        {
            string selectString = "select * from Beams where 梁号 = '" + beanNum + "'";
            DataTable dt = this.GetDataTable(selectString);           
          //  int[] stations = GetWorkStations(Stations);          
            if (dt != null && dt.Rows.Count>0)
            {
                if (gather.MaxTimes.Length == 3)
                {
                    string workMode = dt.Rows[0]["张拉模式"].ToString();

                    grid.Cell(6, 2).Text = dt.Rows[0]["承包单位"].ToString();
                    grid.Cell(6, 6).Text = dt.Rows[0]["监理单位"].ToString();
                    grid.Cell(6, 10).Text = dt.Rows[0]["土建合同号"].ToString();
                    grid.Cell(7, 2).Text = dt.Rows[0]["工程名称"].ToString();
                    grid.Cell(7, 7).Text = dt.Rows[0]["预制梁厂"].ToString();
                    grid.Cell(8, 2).Text = dt.Rows[0]["梁号"].ToString();
                    grid.Cell(8, 7).Text = Convert.ToDateTime(dt.Rows[0]["张拉日期"].ToString()).ToShortDateString();

                    grid.Cell(9, 2).Text = dt.Rows[0]["砼设计强度"].ToString() + "Mpa";
                    grid.Cell(9, 4).Text = dt.Rows[0]["砼实际强度"].ToString() + "Mpa";
                    grid.Cell(9, 7).Text = dt.Rows[0]["弹性模量"].ToString() + "Mpa";
                    grid.Cell(9, 10).Text = dt.Rows[0]["控制应力"].ToString() + "Mpa";

                    string imageName = dt.Rows[0]["图片名称"].ToString();
                    string imagePath = System.Environment.CurrentDirectory;
                    imagePath = imagePath.Insert(imagePath.Length, "\\image\\" + imageName);

                    switch (workMode)
                    {
                        case "单顶单端":
                            {
                                grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                grid.Cell(10, 7).Text = jacks[0].jack_date;
                                grid.Cell(11, 4).Text = "初始行程" + firstScale + "%";
                                grid.Cell(11, 5).Text = "第一行程" + secondScale + "%";
                                InsertImage(Image_Rows, Image_Column, imagePath, imageName);
                                break;
                            }
                        case "双顶双端":
                        case "双顶单端":
                            {
                                grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                grid.Cell(10, 7).Text = jacks[0].jack_date;
                                grid.Cell(11, 2).Text = jacks[1].jack_serial;
                                grid.Cell(11, 7).Text = jacks[1].jack_date;
                                grid.Cell(11, 4).Text = "初始行程" + firstScale + "%";
                                grid.Cell(11, 5).Text = "第一行程" + secondScale + "%";
                                InsertImage(Image_Rows, Image_Column, imagePath, imageName);

                                break;
                            }
                        case "四顶双端":
                        case "四顶单端":
                            {
                                grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                grid.Cell(10, 7).Text = jacks[0].jack_date;
                                grid.Cell(11, 2).Text = jacks[1].jack_serial;
                                grid.Cell(11, 7).Text = jacks[1].jack_date;
                                grid.Cell(12, 2).Text = jacks[2].jack_serial;
                                grid.Cell(12, 7).Text = jacks[2].jack_date;
                                grid.Cell(13, 2).Text = jacks[3].jack_serial;
                                grid.Cell(13, 7).Text = jacks[3].jack_date;

                                grid.Cell(14, 4).Text = "初始行程" + firstScale + "%";
                                grid.Cell(14, 5).Text = "第一行程" + secondScale + "%";

                                InsertImage(Image_Rows, Image_Column, imagePath, imageName);
                                break;
                            }
                    }
                }
                else
                {

                    string workMode = dt.Rows[0]["张拉模式"].ToString();

                    grid.Cell(6, 2).Text = dt.Rows[0]["承包单位"].ToString();
                    grid.Cell(6, 8).Text = dt.Rows[0]["监理单位"].ToString();
                    grid.Cell(6, 12).Text = dt.Rows[0]["土建合同号"].ToString();
                    grid.Cell(7, 2).Text = dt.Rows[0]["工程名称"].ToString();
                    grid.Cell(7, 9).Text = dt.Rows[0]["预制梁厂"].ToString();
                    grid.Cell(8, 2).Text = dt.Rows[0]["梁号"].ToString();
                    grid.Cell(8, 9).Text = Convert.ToDateTime(dt.Rows[0]["张拉日期"].ToString()).ToShortDateString();

                    grid.Cell(9, 2).Text = dt.Rows[0]["砼设计强度"].ToString() + "Mpa";
                    grid.Cell(9, 5).Text = dt.Rows[0]["砼实际强度"].ToString() + "Mpa";
                    grid.Cell(9, 9).Text = dt.Rows[0]["弹性模量"].ToString() + "Mpa";
                    grid.Cell(9, 12).Text = dt.Rows[0]["控制应力"].ToString() + "Mpa";

                    string imageName = dt.Rows[0]["图片名称"].ToString();
                    string imagePath = System.Environment.CurrentDirectory;
                    imagePath = imagePath.Insert(imagePath.Length, "\\image\\" + imageName);

                    switch (workMode)
                    {
                        case "单顶单端":
                            {
                                grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                grid.Cell(10, 7).Text = jacks[0].jack_date;
                                grid.Cell(11, 4).Text = "初始行程" + firstScale + "%";
                                grid.Cell(11, 5).Text = "第一行程" + secondScale + "%";
                                InsertImage(Image_Rows, Image_Column+2, imagePath, imageName);
                                break;
                            }
                        case "双顶双端":
                        case "双顶单端":
                            {
                                grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                grid.Cell(10, 7).Text = jacks[0].jack_date;
                                grid.Cell(11, 2).Text = jacks[1].jack_serial;
                                grid.Cell(11, 7).Text = jacks[1].jack_date;
                                grid.Cell(11, 4).Text = "初始行程" + firstScale + "%";
                                grid.Cell(11, 5).Text = "第一行程" + secondScale + "%";
                                InsertImage(Image_Rows, Image_Column+2, imagePath, imageName);

                                break;
                            }
                        case "四顶双端":
                        case "四顶单端":
                            {
                                grid.Cell(10, 2).Text = jacks[0].jack_serial;
                                grid.Cell(10, 7).Text = jacks[0].jack_date;
                                grid.Cell(11, 2).Text = jacks[1].jack_serial;
                                grid.Cell(11, 7).Text = jacks[1].jack_date;
                                grid.Cell(12, 2).Text = jacks[2].jack_serial;
                                grid.Cell(12, 7).Text = jacks[2].jack_date;
                                grid.Cell(13, 2).Text = jacks[3].jack_serial;
                                grid.Cell(13, 7).Text = jacks[3].jack_date;

                                grid.Cell(14, 4).Text = "初始行程" + firstScale + "%";
                                grid.Cell(14, 5).Text = "第一行程" + secondScale + "%";

                                InsertImage(Image_Rows, Image_Column+2, imagePath, imageName);
                                break;
                            }
                    }

                }
            }
        }
        private Hisdata[,] GetHistoricalData(string beamNum ,int stationNum)
        {
            GetScaleValue(beamNum);
            int[] Stations = GetWorkStations(stationNum);
            int columns = GetNumofStations(beamNum, out jacks);
            int rows = GetNumofHoles(beamNum);

            Hisdata[,] data = new Hisdata[rows, columns];

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    data[i, j] = new Hisdata(beamNum, i, Stations[j], firstScale, secondScale, GetDataTable);
                }
            }
            return data;
        }
        private int[] GetWorkStations(int Stations)
        {
            int[] temp;

            switch (Stations)
            {
                case 1234:
                    {
                        temp = new int[4] { 1, 2, 3, 4 };
                        return temp;
                    }
                case 12:
                    {
                        temp = new int[2] { 1, 2 };
                        return temp;
                    }
                case 34:
                    {
                        temp = new int[2] { 3, 4 };
                        return temp;
                    }
                case 1:
                case 2:
                case 3:
                case 4:
                    {
                        temp = new int[1] { Stations };
                        return temp;
                    }
            }
            return null;
        }
        public List<List<ExcelData>> GetExcelData(string beamNum,int Stations)
        {
            Hisdata[,] datas = GetHistoricalData(beamNum,Stations);            
            List<List<ExcelData>> exceldata = new List<List<ExcelData>>();        
            int[] stations = GetWorkStations(Stations);
            int rowIndex = datas.GetLength(0);
            for (int i = 0; i < rowIndex; i++)
            {
                bool flag = false;
                for (int j = 0; j < exceldata.Count; j++)
                {
                    if (datas[i, 0].HoleName == exceldata[j][0].holeName)
                    {
                        for (int c = 0; c < stations.Length; c++)
                        {
                            exceldata[j][c].flag = true;
                            exceldata[j][c].fiftyscale_2_power = datas[i, c].FiftyScale_Power;
                            exceldata[j][c].fiftyscale_2_length = datas[i, c].FiftyScale_Length;
                            exceldata[j][c].hundredscale_power = datas[i, c].HundredScale_Power;
                            exceldata[j][c].hundredscale_length = datas[i, c].HundredScale_Length;
                        }
                        flag = true;
                    }
                }

                if (flag == false)
                {

                    List<ExcelData> temp = new List<ExcelData>();
                    for (int c = 0; c < stations.Length; c++)
                    {
                        temp.Add(new ExcelData(beamNum, datas[i, c], GetDataTable));
                    }
                    exceldata.Add(temp);
                }
            }
            return exceldata;
        }
                
        public void CreateReport(string beamNum)
        {
            try
            {
                string path = System.Environment.CurrentDirectory;
                path = path.Insert(path.Length, "\\预应力参数.ini");
                Ini ini = new Ini(path);
                this.retraceValue = Convert.ToDouble(ini.ReadValue("预应力参数", "回缩值"));
                this.jack_internal_length = Convert.ToDouble(ini.ReadValue("预应力参数", "顶内伸长"));
            }
            catch (System.FormatException)
            {
            }

            string selectString = "select * from Beams where 梁号 = '" + beamNum + "'";
            DataTable dt = GetDataTable(selectString);
            if (dt != null)
            {
                string path_flex;
                string workMode = dt.Rows[0]["张拉模式"].ToString();
                int Stations = Convert.ToInt32(dt.Rows[0]["张拉仪"].ToString());
               
                switch (workMode)
                {

                    case "双顶单端":
                        {
                            path_flex = System.Environment.CurrentDirectory;
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_2.flx");
                            this.Open(path_flex);
                            List<List<ExcelData>> exceldata = GetExcelData(beamNum, Stations);
                            MakeTableTop(beamNum);
                            currentRow = 13;
                            InsertRecordRows_1(exceldata);
                            MakeTableButtom();
                            break;
                        }
                    case "单顶单端":
                        {
                            path_flex = System.Environment.CurrentDirectory;
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_1.flx");
                            this.Open(path_flex);
                            List<List<ExcelData>> exceldata = GetExcelData(beamNum, Stations);                         
                            MakeTableTop(beamNum);
                            currentRow = 12;
                            InsertRecordRows_1(exceldata);
                            MakeTableButtom();
                            break;
                        }                
                    case "双顶双端":
                            {
                            path_flex = System.Environment.CurrentDirectory;
                            path_flex = path_flex.Insert(path_flex.Length, "\\model_2.flx");
                            this.Open(path_flex);
                            List<List<ExcelData>> exceldata = GetExcelData(beamNum,Stations);
                            MakeTableTop(beamNum);
                            InsertRecordRows_2(exceldata);
                            MakeTableButtom();
                            break;
                            }
                    case "四顶双端":
                            {
                                path_flex = System.Environment.CurrentDirectory;
                                path_flex = path_flex.Insert(path_flex.Length, "\\model_4.flx");
                                this.Open(path_flex);
                                List<List<ExcelData>> exceldata = GetExcelData(beamNum, Stations);
                                MakeTableTop(beamNum);
                                
                                break;
                            }
                }
            }
           
            //string path_flex = System.Environment.CurrentDirectory;
           // path_flex = path_flex.Insert(path_flex.Length, "\\model_2.flx");
            //this.Open(path_flex);
            //List<List<ExcelData>> exceldata = GetExcelData(beamNum);

             //MakeTableTop(beamNum);
          //  InsertRecordRows_2(exceldata);
            SaveFlex(beamNum);
           // SaveAsExel(beamNum);
        }

        
    }
}
