﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace OutExcelDll
{
    public class ExcelData
    {
        public bool flag = false;
        public int StationNum;
        public string holeName;

        public double D_power;
        public double D_length;

        public double firstscale_power;
        public double firstscale_length;

        public double secondscale_power;
        public double secondscale_length;

        public double fiftyscale_1_power;
        public double fiftyscale_1_length;

        public double fiftyscale_2_power;
        public double fiftyscale_2_length;

        public double hundredscale_power;
        public double hundredscale_length;

        public delegate DataTable GetDataTableDelegate(string path);
        public GetDataTableDelegate OnGetTable;
        
        public double TotalLength
        {
            get
            {
                if (flag)
                {
                    return hundredscale_length + fiftyscale_1_length - fiftyscale_2_length + secondscale_length - 2 * firstscale_length;
                }
                else
                {
                    return hundredscale_length + secondscale_length - 2 * firstscale_length;
                }
            }
        }

        public ExcelData(string beamNum, Hisdata temp, GetDataTableDelegate OnGetTable)
        {
            this.holeName = temp.HoleName;
            string selectString = "select * from BeamInformation where 梁号 = '" + beamNum + "' and 序号 = " + temp.SequenceNum;
            DataTable dt = OnGetTable(selectString);
            
            this.D_length = Convert.ToDouble(dt.Rows[0]["设计伸长量"]);
            this.D_power = Convert.ToDouble(dt.Rows[0]["设计张拉力"]);
            this.StationNum = temp.SationNum;
            this.firstscale_power = temp.FirstScale_Power;
            this.firstscale_length = temp.FirstScale_Length;

            this.secondscale_power = temp.SecondScale_Power;
            this.secondscale_length = temp.SecondScale_Length;

            this.fiftyscale_1_power = temp.FiftyScale_Power;
            this.fiftyscale_1_length = temp.FiftyScale_Length;

            this.hundredscale_power = temp.HundredScale_Power;
            this.hundredscale_length = temp.HundredScale_Length;
        }
    }
}
