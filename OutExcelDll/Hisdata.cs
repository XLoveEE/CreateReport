﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace OutExcelDll
{
    public class Hisdata
    {
        private const int FirstScaleRows = 0;
        private const int SecondScaleRows = 1;
        private const int FiftyScaleRows = 2;
        private const int HundredScaleRows = 3;

        private int sequenceNum; //张拉序号
        private int stationNum;
        private string holeName;

        private double firstscale_power;
        private double firstscale_length;

        private double secondscale_power;
        private double secondscale_length;

        private double fiftyscale_power;
        private double fiftyscale_length;

        private double hundredscale_power;
        private double hundredscale_length;

        public delegate DataTable GetDataTableDelegate(string path);
        public GetDataTableDelegate OnGetTable;

        public int SequenceNum
        {
            get { return sequenceNum; }
            set { sequenceNum = value; }
        }

        public int SationNum
        {
            get { return stationNum; }
            set { stationNum = value; }
        }

        public string HoleName
        {
            get { return holeName; }
            set { holeName = value; }
        }

        public double FirstScale_Power
        {
            get { return firstscale_power; }
            set { firstscale_power = value; }
        }

        public double FirstScale_Length
        {
            get { return firstscale_length; }
            set { firstscale_length = value; }
        }

        public double SecondScale_Power
        {
            get { return secondscale_power; }
            set { secondscale_power = value; }
        }

        public double SecondScale_Length
        {
            get { return secondscale_length; }
            set { secondscale_length = value; }
        }

        public double HundredScale_Power
        {
            get { return hundredscale_power; }
            set { hundredscale_power = value; }
        }

        public double HundredScale_Length
        {
            get { return hundredscale_length; }
            set { hundredscale_length = value; }
        }

        public double FiftyScale_Power
        {
            get { return fiftyscale_power; }
            set { fiftyscale_power = value; }
        }
        public double FiftyScale_Length
        {
            get { return fiftyscale_length; }
            set { fiftyscale_length = value; }
        }
        public Hisdata(string beamID, int sequence, int station, int firstscale, int secondscale, GetDataTableDelegate OnGetTable)
        {
            try
            {
                string selectString = "select * from [" + beamID + "_KeyValue] where  站号 = '" + station + "' and 张拉序号 = " + sequence + " and 比例 =" + firstscale + " ORDER BY 比例";
                DataTable dt = OnGetTable(selectString);

                this.sequenceNum = sequence;
                this.stationNum = station;
                this.holeName = dt.Rows[0]["孔道"].ToString();

                this.firstscale_power = Convert.ToDouble(dt.Rows[0]["张拉力"]);
                this.firstscale_length = Convert.ToDouble(dt.Rows[0]["伸长量"]);

                selectString = "select * from [" + beamID + "_KeyValue]  where 站号 = '" + station + "' and 张拉序号 = " + sequence + " and 比例 =" + secondscale + " ORDER BY 比例";

                dt = OnGetTable(selectString);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.secondscale_power = Convert.ToDouble(dt.Rows[0]["张拉力"]);
                    this.secondscale_length = Convert.ToDouble(dt.Rows[0]["伸长量"]);
                }
                selectString = "select * from [" + beamID + "_KeyValue]  where 站号 = '" + station + "' and 张拉序号 = " + sequence + " and 比例 =" + 50 + " ORDER BY 比例";

                dt = OnGetTable(selectString);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.fiftyscale_power = Convert.ToDouble(dt.Rows[0]["张拉力"]);
                    this.fiftyscale_length = Convert.ToDouble(dt.Rows[0]["伸长量"]);
                }

                selectString = "select * from [" + beamID + "_KeyValue]  where 站号 = '" + station + "' and 张拉序号 = " + sequence + " and 比例 =" + 100 + " ORDER BY 比例";
                dt = OnGetTable(selectString);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.hundredscale_power = Convert.ToDouble(dt.Rows[0]["张拉力"]);
                    this.hundredscale_length = Convert.ToDouble(dt.Rows[0]["伸长量"]);
                }
            }
            catch (IndexOutOfRangeException)
            {
            }
        }

    }
}
